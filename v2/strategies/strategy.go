package strategies

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/info"
	"ddzSimple/dbg"
	"ddzSimple/v2/hands"
	"ddzSimple/v2/role"
)

// 根據我的手牌 RNG、蓋著 3 張牌的 RNG、目前叫了幾倍，回傳叫牌倍數。
func BidLandlord(myRNGs, deskRNGs []uint, curStake int) int {
	score17 := cards.GetBidScore(myRNGs)
	score20 := cards.GetBidScore(append(myRNGs, deskRNGs...))

	myStake := 0
	if score17 > 7 {
		myStake = 3
	} else if score17 > 5 {
		if score20 > 7 {
			myStake = 3
		}
		myStake = 2
	} else if score17 > 3 {
		if score20 > 5 {
			myStake = 2
		}
		myStake = 1
	}

	if myStake > curStake {
		return myStake
	}
	return 0
}

// 根據遊戲資訊回傳 [是否出牌]、[出什麼牌]、[輸入有無錯誤]
func Play(gi *info.Info) (bool, *hands.Core, error) {
	cc, err := hands.IsValidHandRNGs(gi.CurCardRNGs)
	if err != nil {
		return false, nil, err
	}
	me := role.New(gi)
	dbg.ShowHands(me.Hands)
	if ok, c := hands.IsLastOne(me.Hands); ok && (me.IsLeadTurn() || c.IsBiggerThan(cc)) {
		// return true, hands.Core2Hand(c), nil
		return true, c, nil
	}

	if ok, cs := hands.IsBombAndOne(me.Hands); ok {
		c, bomb := cs[0], cs[1]
		if me.IsLeadTurn() {
			// return true, hands.Core2Hand(c), nil
			return true, c, nil
		} else if bomb.IsBiggerThan(cc) && hands.IsMaxBomb(bomb.MaxValue, gi.NextRNGs, gi.PrevRNGs) {
			// return true, hands.Core2Hand(bomb), nil
			return true, bomb, nil
		}
	}

	c := me.Discard(cc, gi.PrevRNGs, gi.NextRNGs)
	// return c != nil, hands.Core2Hand(c), nil
	return c != nil, c, nil
}
