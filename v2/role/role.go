package role

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/common/info"
	"ddzSimple/dbg"
	"ddzSimple/v2/hands"
)

type pos int

const (
	prev pos = iota - 1
	me
	next
)

type Role struct {
	Hands     map[category.GroupType][]*hands.Core
	ValueMap  map[uint][]uint
	friendPos pos
	leaderPos pos
}

func New(gi *info.Info) *Role {
	r := &Role{
		Hands:    map[category.GroupType][]*hands.Core{},
		ValueMap: map[uint][]uint{},
	}
	vMap := cards.GetValues(gi.MyRNGs)
	for k, v := range vMap {
		r.ValueMap[k] = v
	}
	r.Hands = hands.Set(vMap)

	r.leaderPos = pos(gi.CurPlayer)
	r.friendPos = pos(gi.FriendOrder)
	return r
}

func (o *Role) IsLeadTurn() bool {
	return o.leaderPos == me
}

// 根據上下家的手牌內容出牌
func (o *Role) Discard(curHand *hands.Core, pRNGs, nRNGs []uint) *hands.Core {

	pMap := cards.GetValues(pRNGs)
	pHands := hands.Set(pMap)
	nMap := cards.GetValues(nRNGs)
	nHands := hands.Set(nMap)
	if o.leaderPos == me {
		if o.friendPos == me {
			dbg.Print("我是地主，我開始出牌")
			if ok, c := o.landlordLeadCheck(pHands, nHands); ok {
				return c
			}
		} else {
			dbg.Print("我是農民，我開始出牌")
			fHands, eHands := pHands, nHands
			if o.friendPos == next {
				fHands, eHands = nHands, pHands
			}
			if ok, c := o.peasantLeadCheck(fHands, eHands); ok {
				return c
			}
		}
		return o.lead(pMap, nMap)
	} else {
		if o.leaderPos == o.friendPos {
			dbg.Print("我是農民，我開始跟友軍牌")
			return o.followFriend(curHand, nHands)
		}

		if o.friendPos == me {
			dbg.Print("我是地主，我開始跟農民牌")
			if ok, c := o.landlordFollow(curHand, pHands, nHands); ok {
				return c
			}
		} else {
			dbg.Print("我是農民，我開始跟地主牌")
			if ok, c := o.followLandlord(curHand, nHands); ok {
				return c
			}
		}
		dbg.Print("跟敵人牌")
		return o.follow(curHand)
	}
}

func (o *Role) landlordLeadCheck(epHands, enHands map[category.GroupType][]*hands.Core) (bool, *hands.Core) {
	lastn, nc := hands.IsLastOne(enHands)
	lastp, pc := hands.IsLastOne(epHands)

	if lastp && lastn { //上下家都各剩一手
		for _, ha := range o.Hands {
			for _, c := range ha {
				if c.IsNotSmallerThan(pc) && c.IsNotSmallerThan(nc) {
					dbg.Print("上下家都各剩一手, 所以我要出牌：%s", c.Message())
					return true, c
				}
			}
		}

		if ok, c := o.forceNotComparable(pc.Count, nc.Count); ok {
			dbg.Print("上下家都各剩一手, 所以我拆牌後出牌：%s", c.Message())
			return true, c
		}
	} else if lastn || lastp {
		ec := pc
		if lastn {
			ec = nc
			dbg.Print("下家只剩一手, 所以我要出牌")
		} else {
			dbg.Print("上家只剩一手, 所以我要出牌")
		}
		return o.forceStopEnemy(ec)
	}
	return false, nil
}

func (o *Role) peasantLeadCheck(pHands, nHands map[category.GroupType][]*hands.Core) (bool, *hands.Core) {
	fHands, eHands := pHands, nHands
	if o.friendPos == next {
		fHands, eHands = nHands, pHands
	}

	lastF, fc := hands.IsLastOne(fHands)
	lastE, ec := hands.IsLastOne(eHands)

	if lastE && lastF { //上下家都各剩一手
		if o.friendPos == next {
			if ok, c := o.forceSmaller(fc); ok {
				dbg.Print("下家同伴只剩一手, 所以我拆牌後出牌讓它脫手：%s", c.Message())
				return true, c
			}
			dbg.Print("下家同伴只剩一手的其他情況")
			return o.forceStopEnemy(ec)
		} else {
			if ec.IsSmallerThan(fc) {
				for _, c := range o.Hands[fc.GroupType] {
					if c.IsSmallerThan(fc) && !c.IsSmallerThan(ec) {
						dbg.Print("上家同伴只剩一手,但下家地主也只剩一手。所以我找牌能讓同伴脫手但地主不能脫手的牌：%s", c.Message())
						return true, c
					}
				}
			} else if !ec.IsComparable(fc) {
				for _, c := range o.Hands[fc.GroupType] {
					if c.IsSmallerThan(fc) {
						dbg.Print("上家同伴只剩一手,但下家地主也只剩一手。所以我找牌能讓同伴脫手但地主不能脫手的牌：%s", c.Message())
						return true, c
					}
				}
			} else {
				dbg.Print("上家同伴只剩一手,但下家地主也只剩一手的其他情況")
				return o.forceStopEnemy(ec)
			}
		}
	} else if lastF {
		if ok, c := o.forceSmaller(fc); ok {
			dbg.Print("同伴只剩一手，所以我找牌能讓同伴脫手的牌：%s", c.Message())
			return true, c
		}
	} else if lastE {
		dbg.Print("地主只剩一手，所以我找牌能阻擋它脫手的牌")
		return o.forceStopEnemy(ec)
	}
	return false, nil
}

func (o *Role) lead(pMap, nMap map[uint][]uint) *hands.Core {
	var hArr []*hands.Core
	for _, vs := range o.Hands {
		hArr = append(hArr, vs...)
	}
	if len(hArr) == 2 {
		chk := []bool{true, true}
		if o.friendPos != prev {
			for i, h := range hArr {
				has, _ := hands.ForceRankGroup(h, pMap, cpB)
				chk[i] = chk[i] && has
			}
		}
		if o.friendPos != next {
			for i, h := range hArr {
				has, _ := hands.ForceRankGroup(h, nMap, cpB)
				chk[i] = chk[i] && has
			}
		}

		for i, b := range chk {
			if b {
				dbg.Print("我只剩兩手，而且這副牌獨大：%s", hArr[i].Message())
				return hArr[i]
			}
		}
	}

	for _, c := range hArr {
		switch c.GroupType {
		case category.FourBomb, category.KingBomb:
			continue
		default:
			if c.MaxValue < cards.A {
				dbg.Print("選到第一副牌點數小於A的牌：%s", c.Message())
				return c
			}
		}
	}
	dbg.Print("隨便選：%s", hArr[0].Message())
	return hArr[0]
}

func (o *Role) landlordFollow(cc *hands.Core, pHands, nHands map[category.GroupType][]*hands.Core) (bool, *hands.Core) {
	ec := cc
	dbg.ShowHands(o.Hands)
	lastn, nc := hands.IsLastOne(nHands)
	if lastn && nc.IsBiggerThan(ec) {
		ec = nc
	}
	lastp, pc := hands.IsLastOne(pHands)
	if lastp && pc.IsBiggerThan(ec) {
		ec = pc
	}
	if ec.IsEqual(cc) {
		if (lastn && o.leaderPos == next) || (lastp && o.leaderPos == prev) {
			if ok, c := o.forceBigger(cc); ok {
				dbg.Print("兩個農民有人要脫手了，但無法上手。所以我選 %s 避免遊戲結束", c.Message())
				return true, c
			}
		}
	} else {
		if ok, c := o.forceBiggerOrEqual(ec); ok {
			dbg.Print("兩個農民有人要脫手了，但可以上手。所以我選 %s 避免遊戲結束", c.Message())
			return true, c
		}
	}

	return false, nil
}

func (o *Role) followLandlord(cc *hands.Core, nHands map[category.GroupType][]*hands.Core) (bool, *hands.Core) {
	if o.friendPos == next {
		if lastF, fc := hands.IsLastOne(nHands); lastF && fc.IsBiggerThan(cc) {
			dbg.Print("下家友軍要脫手了，所以我選擇 pass")
			return true, nil
		}
	} else {
		if lastE, ec := hands.IsLastOne(nHands); lastE {
			if ec.IsBiggerThan(cc) {
				if ok, c := o.forceBiggerOrEqual(ec); ok {
					dbg.Print("下家地主要脫手了，且能壓過當前的牌，所以我選擇 %s", c.Message())
					return true, c
				}
			} else {
				if ok, c := o.forceBigger(cc); ok {
					dbg.Print("下家地主要脫手了，但不能壓過當前的牌，所以我選擇 %s", c.Message())
					return true, c
				}
			}
		}
	}
	return false, nil
}

func (o *Role) followFriend(cc *hands.Core, nHands map[category.GroupType][]*hands.Core) *hands.Core {
	lastN, nc := hands.IsLastOne(nHands)
	if lastN {
		if o.friendPos == next {
			dbg.Print("下家友軍要脫手了，所以我選擇 pass")
			return nil
		} else if nc.IsBiggerThan(cc) {
			if ok, c := o.forceBiggerOrEqual(nc); ok {
				dbg.Print("下家敵軍要脫手了，所以我選擇%s", c.Message())
				return c
			}
		}
	}

	handCount := 0
	for _, cs := range o.Hands {
		handCount += len(cs)
	}

	for _, c := range o.Hands[cc.GroupType] {
		if c.IsBiggerThan(cc) {
			if (handCount <= 2 || o.friendPos == prev) && c.MaxValue < cards.A {
				dbg.Print("[手牌組數是否 <= 2(%t)或友軍是否為上家(%t)] 且 [點數是否小於A(%t)]，所以我選擇%s", handCount <= 2, o.friendPos == prev, c.MaxValue < cards.A, c.Message())
				return c
			}
		}
	}
	return nil
}

func (o *Role) follow(cc *hands.Core) *hands.Core {
	for _, c := range o.Hands[cc.GroupType] {
		if c.IsBiggerThan(cc) {
			dbg.Print("一般跟牌，所以我選擇%s", c.Message())
			return c
		}
	}

	if cc.MaxValue == cards.TWO {
		if cc.GroupType == category.Solo {
			if len(o.Hands[category.KingBomb]) > 0 {
				c := o.Hands[category.KingBomb][0]
				c.Count--
				c.GroupType = category.Solo
				delete(c.Group, cards.BJ)
				c.MinValue = c.MaxValue
				dbg.Print("對方出單張2，所以我選擇%s", c.Message())
				return c
			}
		} else {
			if len(o.Hands[category.FourBomb]) > 0 && cc.Count < 4 {
				dbg.Print("對方出%d張2，但我有四炸，所以我選擇%s", cc.Count, o.Hands[category.FourBomb][0].Message())
				return o.Hands[category.FourBomb][0]
			}
			if len(o.Hands[category.KingBomb]) > 0 {
				dbg.Print("對方出多張2，但我有王炸，所以我選擇%s", o.Hands[category.KingBomb][0].Message())
				return o.Hands[category.KingBomb][0]
			}
		}
		dbg.Print("對方出2的其他情況，選擇pass")
		return nil
	}

	if cc.MaxValue == cards.A {
		if len(o.ValueMap[cards.TWO]) >= cc.Count {
			c := hands.NewSameValue(cards.TWO, o.ValueMap[cards.TWO][:cc.Count])
			dbg.Print("對方出多張A，但我有多張2，所以我選擇%s", c.Message())
			return c
			// return hands.NewSameValue(cards.TWO, o.ValueMap[cards.TWO][:cc.Count])
		}
		if cc.Count > 1 && cc.Count < 4 && len(o.Hands[category.FourBomb]) > 0 {
			dbg.Print("對方出%d張A有關，但我有四炸，所以我選擇%s", cc.Count, o.Hands[category.FourBomb][0].Message())
			return o.Hands[category.FourBomb][0]
		}
		if cc.Count > 3 && len(o.Hands[category.KingBomb]) > 0 {
			dbg.Print("對方出3張以上A有關，但我有王炸，所以我選擇%s", o.Hands[category.KingBomb][0].Message())
			return o.Hands[category.KingBomb][0]
		}
		dbg.Print("對方出A的其他情況，選擇pass")
		return nil
	}

	dbg.Print("開始軟性拆牌")
	return hands.SplitHand(cc, o.Hands, cpB)
}
