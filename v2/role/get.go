package role

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/v2/hands"
)

func cpBE(i, j uint) bool {
	return i >= j
}

func cpB(i, j uint) bool {
	return i > j
}

func cpE(i, j uint) bool {
	return i == j
}

func cpS(i, j uint) bool {
	return i < j
}

func cpN(i, j uint) bool {
	return i != j
}

// 強迫找出比指定目標還大的手牌
func (o *Role) forceBigger(h *hands.Core) (bool, *hands.Core) {
	if h.GroupType == category.KingBomb {
		return false, nil
	}

	for _, c := range o.Hands[h.GroupType] {
		if c.IsBiggerThan(h) {
			return true, c
		}
	}

	if h.GroupType != category.FourBomb {
		if len(o.Hands[category.FourBomb]) > 0 {
			return true, o.Hands[category.FourBomb][0]
		}
	}

	if len(o.Hands[category.KingBomb]) > 0 {
		return true, o.Hands[category.KingBomb][0]
	}

	return hands.ForceRankGroup(h, o.ValueMap, cpB)
}

// 強迫找出比指定目標還大或相同的手牌
func (o *Role) forceBiggerOrEqual(h *hands.Core) (bool, *hands.Core) {
	for _, c := range o.Hands[h.GroupType] {
		if c.IsEqual(h) {
			return true, c
		}
	}

	if ok, c := hands.ForceRankGroup(h, o.ValueMap, cpE); ok {
		return true, c
	}

	return o.forceBigger(h)
}

// 強迫找出不能和指定目標比較的手牌
func (o *Role) forceNotComparable(eCounts ...int) (bool, *hands.Core) {
	cnt1 := 0
	cnt2 := 0
	for _, c := range eCounts {
		switch c {
		case 1:
			cnt1++
		case 2:
			cnt2++
		}
	}

	cnt := 0
	if cnt1 == 0 {
		cnt = 1
	} else if cnt2 == 0 {
		cnt = 2
	} else {
		cnt = 3
	}

	for v := uint(3); v <= cards.RJ; v++ {
		rs := o.ValueMap[v]
		if len(rs) >= cnt {
			return true, hands.NewSameValue(v, rs[:cnt])
		}
	}
	return false, nil
}

// 強迫找出比指定目標還小的手牌
func (o *Role) forceSmaller(fc *hands.Core) (bool, *hands.Core) {
	switch fc.GroupType {
	case category.KingBomb, category.FourBomb:
		for v, rs := range o.ValueMap {
			return true, hands.NewSameValue(v, rs[:1])
		}
	default:
		for _, h := range o.Hands[fc.GroupType] {
			if h.IsSmallerThan(fc) {
				return true, h
			}
		}
	}

	return hands.ForceRankGroup(fc, o.ValueMap, cpS)
}

// 強迫找出敵軍無法出牌的牌
func (o *Role) forceStopEnemy(ec *hands.Core) (bool, *hands.Core) {
	for gt, ha := range o.Hands {
		if gt == ec.GroupType {
			for _, c := range ha {
				if !c.IsSmallerThan(ec) {
					return true, c
				}
			}
		} else {
			return true, ha[0]
		}
	}

	return o.forceNotComparable(ec.Count)
}
