package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"fmt"
	"sort"
)

// 牌型核心內容
type Core struct {
	GroupType category.GroupType //牌型
	Group     map[uint][]uint
	Count     int
	MaxValue  uint
	MinValue  uint
}

// 回傳王炸牌型
func NewKingBomb(bj, rj uint) *Core {
	if bj > rj {
		rj, bj = bj, rj
	}
	r := &Core{
		Group:     map[uint][]uint{},
		GroupType: category.KingBomb,
		MinValue:  cards.BJ,
		MaxValue:  cards.RJ,
		Count:     2,
	}
	r.Group[cards.RJ] = []uint{rj}
	r.Group[cards.BJ] = []uint{bj}

	return r
}

func NewSameValue(v uint, rngs []uint) *Core {
	r := &Core{
		GroupType: category.Unknown,
		Group:     map[uint][]uint{},
		Count:     len(rngs),
		MaxValue:  v,
		MinValue:  v,
	}
	r.Group[v] = rngs
	switch r.Count {
	case 1:
		r.GroupType = category.Solo
	case 2:
		r.GroupType = category.Pair
	case 3:
		r.GroupType = category.Trio
	default:
		r.GroupType = category.FourBomb
	}
	return r
}

func NewStraight(gt category.GroupType, vs, rngs []uint) *Core {
	vMap := make(map[uint][]uint)
	var vSort []int
	for i, v := range vs {
		vMap[v] = append(vMap[v], rngs[i])
		vSort = append(vSort, int(v))
	}
	sort.Ints(vSort)
	r := &Core{
		GroupType: category.Unknown,
		Group:     map[uint][]uint{},
		Count:     len(rngs),
		MaxValue:  uint(vSort[len(vSort)-1]),
		MinValue:  uint(vSort[0]),
	}
	r.GroupType = gt

	return r
}

func MergeWithKicker(mgt category.GroupType, kickers []*Core, master *Core) *Core {
	r := &Core{
		GroupType: mgt,
		Group:     map[uint][]uint{},
		Count:     master.Count,
		MaxValue:  master.MaxValue,
		MinValue:  master.MinValue,
	}

	for v, rngs := range master.Group {
		r.Group[v] = rngs
	}

	for _, k := range kickers {
		r.Count += k.Count
		r.Group[k.MaxValue] = k.Group[k.MaxValue]
	}

	return r
}

func (o *Core) Message() string {
	cat := o.GroupType.ToString()
	rank := cards.Rank(o.MaxValue)
	mc := "["
	kc := ""

	for i := uint(3); i <= cards.RJ; i++ {
		t := ""
		for _, r := range o.Group[i] {
			t += cards.Name(r) + ","
		}
		if i >= o.MinValue && i <= o.MaxValue {
			mc += t
		} else {
			kc += t
		}
	}

	mc += "]"
	if len(kc) > 0 {
		kc = "+[" + kc + "]"
	}

	return fmt.Sprintf("牌型: %s-%s，總共有 %d 張牌，組成內容：%s", cat, rank, o.Count, mc+kc)
}

func (o *Core) RNGs() []uint {
	var rngs []uint
	for _, rs := range o.Group {
		rngs = append(rngs, rs...)
	}
	return rngs
}
