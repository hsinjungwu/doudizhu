package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
)

func SplitHand(tHand *Core, hMap map[category.GroupType][]*Core, cp cpFunc) *Core {
	switch tHand.GroupType {
	case category.Solo, category.Pair, category.Trio:
		return splitSameValue(tHand, hMap, cp)
	case category.Trio1, category.Trio2:
		return splitTrioPlus(tHand, hMap)
	case category.Chain, category.PairsChain, category.Airplane:
		return splitChain(tHand, hMap)
	default:
		return nil
	}
}

func splitSameValue(tHand *Core, hMap map[category.GroupType][]*Core, cp cpFunc) *Core {
	var sgts []category.GroupType
	invalidCount := 0
	size := 0
	switch tHand.GroupType {
	case category.Solo:
		sgts = []category.GroupType{category.Chain, category.Trio, category.Pair}
		invalidCount, size = 5, 1
	case category.Pair:
		sgts = []category.GroupType{category.PairsChain, category.Airplane, category.Trio}
		invalidCount, size = 6, 2
	case category.Trio:
		sgts = []category.GroupType{category.Airplane}
		invalidCount, size = 6, 3
	}

	vMap := make(map[uint][]uint)
	for _, sgt := range sgts {
		for _, h := range hMap[sgt] {
			if h.Count == invalidCount || h.MaxValue <= tHand.MaxValue {
				continue
			}
			if h.MinValue > tHand.MaxValue {
				vMap[h.MinValue] = h.Group[h.MinValue][:size]
			} else {
				vMap[h.MaxValue] = h.Group[h.MaxValue][:size]
			}
		}
	}

	for v := tHand.MaxValue + 1; v < cards.BJ; v++ {
		if len(vMap[v]) > 0 {
			return NewSameValue(v, vMap[v])
		}
	}
	return nil
}

func splitChain(tHand *Core, hMap map[category.GroupType][]*Core) *Core {
	minLengh := 13
	minValue := cards.TWO
	idx := -1

	for i, mh := range hMap[tHand.GroupType] {
		if mh.Count > tHand.Count && mh.MaxValue > tHand.MaxValue {
			if mh.Count < minLengh || (mh.Count == minLengh && mh.MaxValue < minValue) {
				idx = i
				minLengh = mh.Count
				minValue = mh.MaxValue
			}
		}
	}

	if idx == -1 {
		return nil
	}
	result := hMap[tHand.GroupType][idx]
	for v := range result.Group {
		if v <= tHand.MinValue {
			delete(result.Group, v)
			result.MinValue++
		}
	}
	for len(result.Group) > len(tHand.Group) {
		delete(result.Group, result.MaxValue)
		result.MaxValue--
	}
	result.Count = tHand.Count
	return result
}

func splitTrioPlus(tHand *Core, hMap map[category.GroupType][]*Core) *Core {
	vMap := make(map[uint][]uint)
	kMap := make(map[uint][]uint)
	sgs := []*Core{}
	switch tHand.GroupType {
	case category.Trio1:
		sgs = hMap[category.Airplane1]
	case category.Trio2:
		sgs = hMap[category.Airplane2]
	}

	for _, mh := range sgs {
		for kv, krs := range mh.Group {
			if len(krs) < 3 {
				kMap[kv] = krs
			}
		}
		if mh.MaxValue-mh.MinValue == 1 {
			continue
		}
		if mh.MinValue > tHand.MaxValue {
			vMap[mh.MinValue] = mh.Group[mh.MinValue]
		} else if mh.MaxValue > tHand.MaxValue {
			vMap[mh.MaxValue] = mh.Group[mh.MaxValue]
		}
	}

	if len(vMap) == 0 {
		return nil
	}

	result := &Core{
		GroupType: tHand.GroupType,
		Group:     map[uint][]uint{},
		Count:     tHand.Count,
		MaxValue:  0,
		MinValue:  0,
	}

	for v := tHand.MaxValue + 1; v <= cards.TWO; v++ {
		if len(vMap[v]) > 0 {
			result.Group[v] = vMap[v]
			result.MaxValue, result.MinValue = v, v
			break
		}
	}

	for v := uint(3); v <= cards.TWO; v++ {
		if len(kMap[v]) > 0 {
			result.Group[v] = kMap[v]
			break
		}
	}
	return result
}
