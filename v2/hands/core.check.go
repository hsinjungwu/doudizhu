package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"fmt"
)

// 手牌是否可組成唯一一組
func IsLastOne(hMap map[category.GroupType][]*Core) (bool, *Core) {
	if len(hMap) > 2 {
		return false, nil
	}

	if len(hMap) == 1 {
		for _, his := range hMap {
			if len(his) == 1 {
				return true, his[0]
			}
		}
		return false, nil
	}

	sn := len(hMap[category.Solo])
	pn := len(hMap[category.Pair])

	if len(hMap[category.FourBomb]) == 1 {
		m := hMap[category.FourBomb][0]
		if sn == 2 {
			return true, MergeWithKicker(category.Four1, hMap[category.Solo], m)
		} else if pn == 2 {
			return true, MergeWithKicker(category.Four2, hMap[category.Pair], m)
		}
	}

	if len(hMap[category.Trio]) == 1 {
		m := hMap[category.Trio][0]
		if sn == 1 {
			return true, MergeWithKicker(category.Trio1, hMap[category.Solo], m)
		} else if pn == 1 {
			return true, MergeWithKicker(category.Trio2, hMap[category.Pair], m)
		}
	}

	return false, nil
}

// 剩下的牌型是否為任意一組+炸彈或王炸
func IsBombAndOne(hMap map[category.GroupType][]*Core) (bool, []*Core) {
	copyMap := make(map[category.GroupType][]*Core)
	for gt, his := range hMap {
		copyMap[gt] = his
	}
	result := make([]*Core, 2)

	if len(copyMap[category.KingBomb]) == 1 {
		result[1] = copyMap[category.KingBomb][0]
		delete(copyMap, category.KingBomb)
	} else if len(copyMap[category.FourBomb]) > 0 {
		l := len(copyMap[category.FourBomb])
		result[1] = copyMap[category.FourBomb][l-1]
		if l > 1 {
			copyMap[category.FourBomb] = copyMap[category.FourBomb][:l-1]
		} else {
			delete(copyMap, category.FourBomb)
		}
	} else {
		return false, nil
	}

	if ok, h := IsLastOne(copyMap); ok {
		result[0] = h
		return true, result
	}
	return false, nil
}

func IsMaxBomb(myBombV uint, others ...[]uint) bool {
	for _, orngs := range others {
		ov := cards.GetValues(orngs)
		if len(ov[cards.RJ])+len(ov[cards.BJ]) == 2 {
			return false
		}

		for v := myBombV + 1; v <= cards.TWO; v++ {
			if len(ov[v]) == 4 {
				return false
			}
		}
	}
	return true
}

func IsValidHandRNGs(rngs []uint) (*Core, error) {
	r := &Core{
		Group:     map[uint][]uint{},
		Count:     0,
		GroupType: category.Unknown,
		MinValue:  0,
		MaxValue:  0,
	}
	if len(rngs) == 0 {
		return r, nil
	}

	vMap := cards.GetValues(rngs)
	errLog := fmt.Sprintf("錯誤 RNGs = %v, vMap = %v\n", rngs, vMap)
	r.Group = vMap
	r.Count = len(rngs)

	if len(vMap) == 2 && len(vMap[cards.BJ])+len(vMap[cards.RJ]) == 2 {
		r.MaxValue = cards.RJ
		r.MinValue = cards.BJ
		r.GroupType = category.KingBomb
		return r, nil
	}

	nCount := make([][]uint, 5)
	nLength := make([]uint, 5)
	for i := 0; i < 5; i++ {
		nCount[i] = []uint{}
	}
	for v, rs := range vMap {
		l := len(rs)
		nCount[l] = append(nCount[l], v)
		nLength[l]++
	}

	if len(vMap) == 1 {
		r.MaxValue = nCount[r.Count][0]
		r.MinValue = r.MaxValue
		switch r.Count {
		case 1:
			r.GroupType = category.Solo
		case 2:
			r.GroupType = category.Pair
		case 3:
			r.GroupType = category.Trio
		case 4:
			r.GroupType = category.FourBomb
		default:
			errLog += fmt.Sprintf("相同的 v = %d, 有 %d 個 RNG", r.MaxValue, r.Count)
			return nil, fmt.Errorf(errLog)
		}
		return r, nil
	}

	if nLength[1]*nLength[2] > 0 || nLength[4] > 1 || nLength[4]*nLength[3] > 0 {
		errLog += fmt.Sprint("發生無效的情況\n")
		return nil, fmt.Errorf(errLog)
	}

	if nLength[4] == 1 {
		if nLength[1]+nLength[2] != 2 {
			errLog += fmt.Sprint("檢查四帶發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}
		r.MaxValue = nCount[4][0]
		r.MinValue = r.MaxValue
		r.GroupType = category.Four1
		if nLength[2] == 2 {
			r.GroupType = category.Four2
		}
		return r, nil
	}

	if nLength[3] == 1 {
		if nLength[1]+nLength[2] != 1 {
			errLog += fmt.Sprint("檢查三帶發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}
		r.MaxValue = nCount[3][0]
		r.MinValue = r.MaxValue
		r.GroupType = category.Trio1
		if nLength[2] == 1 {
			r.GroupType = category.Trio2
		}
		return r, nil
	}

	if nLength[3] >= 2 {
		if b, min, max := checkStraight(vMap, 3, nLength[3]); b {
			r.MaxValue = max
			r.MinValue = min
			if nLength[1] == nLength[3] {
				r.GroupType = category.Airplane1
			} else if nLength[2] == nLength[3] {
				r.GroupType = category.Airplane2
			} else if nLength[1]+nLength[2] == 0 {
				r.GroupType = category.Airplane
			} else {
				errLog += fmt.Sprint("飛機組合發生錯誤\n")
				return nil, fmt.Errorf(errLog)
			}
			return r, nil
		} else {
			errLog += fmt.Sprint("檢查飛機主體發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}
	}
	if nLength[1] >= 5 {
		if b, min, max := checkStraight(vMap, 1, nLength[1]); b {
			r.MaxValue = max
			r.MinValue = min
			r.GroupType = category.Chain
			return r, nil
		} else {
			errLog += fmt.Sprint("檢查順子發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}
	} else if nLength[2] >= 3 {
		if b, min, max := checkStraight(vMap, 2, nLength[2]); b {
			r.MaxValue = max
			r.MinValue = min
			r.GroupType = category.PairsChain
			return r, nil
		} else {
			errLog += fmt.Sprint("檢查連對發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}
	} else {
		errLog += fmt.Sprintf("長度有誤發生錯誤 cnt[1] = %d, cnt[2]=%d\n", nLength[1], nLength[2])
		return nil, fmt.Errorf(errLog)
	}
}

func checkStraight(vMap map[uint][]uint, size int, cnt uint) (bool, uint, uint) {
	fv := uint(3)
	for len(vMap[fv]) != size || fv > cards.A {
		fv++
	}
	b := true
	for v := uint(1); v < cnt; v++ {
		b = b && len(vMap[fv+v]) == size
	}
	if b {
		return true, fv, fv + cnt - 1
	}
	return false, 0, 0
}
