package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
)

type cpFunc func(uint, uint) bool

// 強拆 RNG 建立指定牌型的牌
func ForceRankGroup(eHand *Core, vMap map[uint][]uint, cp cpFunc) (bool, *Core) {
	switch eHand.GroupType {
	case category.Solo, category.Pair, category.Trio:
		return forceSameValue(eHand.MaxValue, eHand.Count, vMap, cp)
	case category.Trio1, category.Trio2:
		return forceTrioPlus(eHand, vMap, cp)
	case category.Chain, category.PairsChain, category.Airplane:
		return forceStraight(eHand.GroupType, len(eHand.Group), eHand.MinValue, vMap, cp)
	case category.Airplane1, category.Airplane2:
		return forceAirplanPlus(eHand, vMap, cp)
	case category.Four1, category.Four2:
		return forceFourPlus(eHand, vMap, cp)
	}

	return false, nil
}

// 根據手牌 rng 強制拆出單張、對子、三條或四炸
func forceSameValue(rank uint, count int, vMap map[uint][]uint, cp cpFunc) (bool, *Core) {
	for v := uint(3); v <= cards.RJ; v++ {
		if cp(v, rank) && len(vMap[v]) >= count {
			return true, NewSameValue(v, vMap[v][:count])
		}
	}
	return false, nil
}

// 根據手牌 rng 強制拆出單順、連對或飛機
func forceStraight(gt category.GroupType, chainLength int, minS uint, vMap map[uint][]uint, fc cpFunc) (bool, *Core) {
	size := 0
	switch gt {
	case category.Chain:
		size = 1
	case category.PairsChain:
		size = 2
	case category.Airplane:
		size = 3
	}

	for minV := uint(3); minV < cards.A; minV++ {
		sizeOK := fc(minV, minS)
		for j := uint(0); j < uint(chainLength); j++ {
			sizeOK = sizeOK && len(vMap[minV+j]) >= size
			if !sizeOK {
				minV += j
				break
			}
		}
		if sizeOK {
			r := &Core{
				GroupType: gt,
				Group:     map[uint][]uint{},
				Count:     chainLength * size,
				MaxValue:  minV + uint(chainLength) - 1,
				MinValue:  minV,
			}
			for i := r.MinValue; i <= r.MaxValue; i++ {
				r.Group[i] = vMap[i][:size]
			}
			return true, r
		}
	}
	return false, nil
}

// 根據手牌 rng 強制拆出三帶一或三帶二
func forceTrioPlus(tHand *Core, vMap map[uint][]uint, cp cpFunc) (bool, *Core) {
	if ok, r := forceSameValue(tHand.MaxValue, 3, vMap, cp); ok {
		if okK := setKicker(r, vMap, tHand.Count-r.Count, 1); okK {
			r.GroupType = tHand.GroupType
			r.Count = tHand.Count
			return true, r
		}
	}
	return false, nil
}

// 根據手牌 rng 強制拆出飛機一或飛機二
func forceAirplanPlus(tHand *Core, vMap map[uint][]uint, cp cpFunc) (bool, *Core) {
	cnt := tHand.Count / 4
	dc := 1
	if tHand.GroupType == category.Airplane2 {
		cnt = tHand.Count / 5
		dc = 2
	}

	if ok, r := forceStraight(category.Airplane, cnt, tHand.MinValue, vMap, cp); ok {
		if okK := setKicker(r, vMap, dc, cnt); okK {
			r.GroupType = tHand.GroupType
			r.Count = tHand.Count
			return true, r
		}
	}
	return false, nil
}

// 根據手牌 rng 強制拆出四帶一或四帶二
func forceFourPlus(tHand *Core, vMap map[uint][]uint, cp cpFunc) (bool, *Core) {
	if okM, r := forceSameValue(tHand.MaxValue, 4, vMap, cp); okM {
		if okK := setKicker(r, vMap, (tHand.Count-r.Count)/2, 2); okK {
			r.GroupType = tHand.GroupType
			r.Count = tHand.Count
			return true, r
		}
	}
	return false, nil
}

func setKicker(r *Core, vMap map[uint][]uint, kSize, kCount int) bool {
	for v := uint(3); v <= cards.TWO; v++ {
		if (v > r.MaxValue || v < r.MinValue) && len(vMap[v]) >= kSize {
			r.Group[v] = vMap[v][:kSize]
			kCount--
			if kCount == 0 {
				return true
			}
		}
	}
	return false
}
