package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"sort"
)

func Set(valueMap map[uint][]uint) map[category.GroupType][]*Core {
	r := initHands(valueMap)
	merge(r, cards.A)
	for gt := range r {
		sortSameType(r[gt])
	}
	return r
}

// 基本理牌
func initHands(valueMap map[uint][]uint) map[category.GroupType][]*Core {
	result := make(map[category.GroupType][]*Core)

	if len(valueMap[cards.BJ])+len(valueMap[cards.RJ]) == 2 {
		bj, rj := valueMap[cards.BJ][0], valueMap[cards.RJ][0]
		c := NewKingBomb(bj, rj)
		result[c.GroupType] = append(result[c.GroupType], c)
		delete(valueMap, cards.BJ)
		delete(valueMap, cards.RJ)
	}

	// for v, rs := range valueMap {
	// 	if len(rs) == 4 {
	// 		c := NewSameValue(v, rs)
	// 		result[c.GroupType] = append(result[c.GroupType], c)
	// 		delete(valueMap, v)
	// 	}
	// }

	straights := []*Core{}
	longStraights := findLongStraight(valueMap)
	for _, c := range longStraights {
		straights = append(straights, c)
	}

	shortStraights := findShortStraight(valueMap)
	for _, c := range shortStraights {
		straights = append(straights, c)
	}

	for _, c := range straights {
		minCount := 0
		switch c.GroupType {
		case category.Chain:
			minCount = 5
		case category.PairsChain:
			minCount = 6
		}
		for c.Count > minCount {
			v := uint(0)
			if len(valueMap[c.MinValue]) > 0 {
				v = c.MinValue
				c.MinValue++
			} else if len(valueMap[c.MaxValue]) > 0 {
				v = c.MaxValue
				c.MaxValue--
			}
			if v > 0 {
				l := len(c.Group[v])
				valueMap[v] = append(valueMap[v], c.Group[v]...)
				c.Count -= l
				delete(c.Group, v)
			} else {
				break
			}
		}
		if c.Count > 0 {
			if c.GroupType == category.Airplane && c.Count == 3 {
				c.GroupType = category.Trio
			}
			result[c.GroupType] = append(result[c.GroupType], c)
		}
	}

	//搜尋剩下的點數
	for v, rs := range valueMap {
		if len(rs) > 0 {
			c := NewSameValue(v, rs)
			result[c.GroupType] = append(result[c.GroupType], c)
		}
		delete(valueMap, v)
	}
	return result
}

func findLongStraight(valueMap map[uint][]uint) []*Core {
	var result []*Core
	for v := uint(3); v <= 10; v++ {
		min := 4
		for i := uint(0); i < 5; i++ {
			if min > len(valueMap[v+i]) {
				min = len(valueMap[v+i])
			}
			if min == 0 {
				v += i
				break
			}
		}
		for l := 0; l < min; l++ {
			r := &Core{
				GroupType: category.Chain,
				Group:     map[uint][]uint{},
				Count:     5,
				MaxValue:  v + 4,
				MinValue:  v,
			}
			for i := r.MinValue; i <= r.MaxValue; i++ {
				r.Group[i] = valueMap[i][:1]
				valueMap[i] = valueMap[i][1:]
			}
			result = append(result, r)
		}
	}

	for i := 0; i < len(result); i++ { //將剩餘牌盡量延長
		for result[i].MaxValue < cards.A {
			nv := result[i].MaxValue + 1
			if len(valueMap[nv]) == 0 {
				break
			}
			result[i].Group[nv] = append(result[i].Group[nv], valueMap[nv][0])
			result[i].Count++
			result[i].MaxValue = nv
			valueMap[nv] = valueMap[nv][1:]
		}
	}

	//單順對接
	ls := len(result)
	for i := 0; i < ls; i++ {
		min, max := uint(0), uint(0)
		for j := i + 1; j < ls; j++ {
			if result[i].MaxValue+1 == result[j].MinValue {
				min, max = result[i].MinValue, result[j].MaxValue
			} else if result[j].MaxValue+1 == result[i].MinValue {
				min, max = result[j].MinValue, result[i].MaxValue
			}
			if min > 0 {
				result[i].Count += result[j].Count
				for v, rs := range result[j].Group {
					result[i].Group[v] = rs
				}
				result[i].MaxValue = max
				result[i].MinValue = min
				result[j] = result[ls-1]
				ls--
				break
			}
		}
	}
	result = result[:ls]

	for i := 0; i < ls; i++ {
		for j := i + 1; j < ls; j++ {
			if result[i].MinValue == result[j].MinValue && result[i].MaxValue == result[j].MaxValue {
				result[i].GroupType = category.PairsChain
				result[i].Count += result[j].Count
				for v := range result[i].Group {
					result[i].Group[v] = append(result[i].Group[v], result[j].Group[v][0])
				}
				result[j] = result[ls-1]
				ls--
				break
			}
		}
	}
	result = result[:ls]

	for i := 0; i < ls; i++ {
		for j := i + 1; j < ls; j++ {
			if result[i].MinValue == result[j].MinValue && result[i].MaxValue == result[j].MaxValue {
				if result[i].GroupType == category.Solo {
					result[i], result[j] = result[j], result[i]
				}
				result[i].GroupType = category.Airplane
				result[i].Count += result[j].Count
				for v := range result[i].Group {
					result[i].Group[v] = append(result[i].Group[v], result[j].Group[v][0])
				}
				result[j] = result[ls-1]
				ls--
				break
			}
		}
	}
	return result[:ls]
}

func findShortStraight(valueMap map[uint][]uint) []*Core {
	var result []*Core
	for size := 3; size > 1; size-- { //該牌的張數
		gt := category.Airplane
		if size == 2 {
			gt = category.PairsChain
		}

		for ln := uint(4); ln > 4-uint(size); ln-- { //牌組長度
			for v := uint(3); v <= 15-ln; v++ {
				sizeOK := true
				for j := uint(0); j < ln; j++ {
					sizeOK = sizeOK && len(valueMap[v+j]) >= size
					if !sizeOK {
						v += j
						break
					}
				}
				if sizeOK {
					r := &Core{
						GroupType: gt,
						Group:     map[uint][]uint{},
						Count:     size * int(ln),
						MaxValue:  v + ln - 1,
						MinValue:  v,
					}
					for i := r.MinValue; i <= r.MaxValue; i++ {
						r.Group[i] = valueMap[i][:size]
						valueMap[i] = valueMap[i][size:]
					}
					result = append(result, r)
					v += ln - 1
				}
			}
		}
	}
	return result
}

func merge(agMap map[category.GroupType][]*Core, kickerMax uint) {
	for gt := range agMap {
		switch gt {
		case category.Airplane, category.Trio, category.Pair, category.Solo:
			sortSameType(agMap[gt])
		}
	}

	for la := len(agMap[category.Airplane]) - 1; la >= 0; la-- {
		ags := agMap[category.Airplane]
		l := len(ags[la].Group)
		sgs := []*Core{}
		sTmp := []*Core{}
		for _, c := range agMap[category.Solo] {
			if (c.MaxValue > ags[la].MaxValue || c.MaxValue < ags[la].MinValue) && c.MaxValue < kickerMax {
				sgs = append(sgs, c)
			} else {
				sTmp = append(sTmp, c)
			}
		}
		pgs := agMap[category.Pair]
		if len(sgs) >= l {
			r := MergeWithKicker(category.Airplane1, sgs[:l], ags[la])
			agMap[category.Airplane1] = append(agMap[category.Airplane1], r)
			agMap[category.Solo] = append(sgs[l:], sTmp...)
			agMap[category.Airplane] = append(ags[:la], ags[la+1:]...)
		} else if len(pgs) >= l && pgs[l-1].MaxValue < kickerMax {
			r := MergeWithKicker(category.Airplane2, pgs[:l], ags[la])
			agMap[category.Airplane2] = append(agMap[category.Airplane2], r)
			agMap[category.Pair] = pgs[l:]
			agMap[category.Airplane] = append(ags[:la], ags[la+1:]...)
		}
	}

	for lt := len(agMap[category.Trio]) - 1; lt >= 0; lt-- {
		tgs := agMap[category.Trio]
		l := len(tgs[lt].Group)
		sgs := agMap[category.Solo]
		pgs := agMap[category.Pair]
		if len(sgs) >= l && sgs[l-1].MaxValue <= kickerMax {
			r := MergeWithKicker(category.Trio1, sgs[:l], tgs[lt])
			agMap[category.Trio1] = append(agMap[category.Trio1], r)
			agMap[category.Solo] = sgs[l:]
			agMap[category.Trio] = append(tgs[:lt], tgs[lt+1:]...)
		} else if len(pgs) >= l && pgs[l-1].MaxValue <= kickerMax {
			r := MergeWithKicker(category.Trio2, pgs[:l], tgs[lt])
			agMap[category.Trio2] = append(agMap[category.Trio2], r)
			agMap[category.Pair] = pgs[l:]
			agMap[category.Trio] = append(tgs[:lt], tgs[lt+1:]...)
		}
	}
	for k, v := range agMap {
		if len(v) == 0 {
			delete(agMap, k)
		}
	}
}

// 排序。比較最大點數由小到大，點數相同則短到長
func sortSameType(arr []*Core) {
	sort.Slice(arr, func(i1, i2 int) bool {
		c1 := arr[i1]
		c2 := arr[i2]
		if c1.MaxValue == c2.MaxValue {
			return c1.Count < c2.Count
		}
		return c1.MaxValue < c2.MaxValue
	})
}
