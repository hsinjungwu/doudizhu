package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"fmt"
)

// 儲存牌組
type Hand struct {
	Group     map[uint][]uint    //點數對應表, key 點數 / values RNG
	Count     int                //RNG 個數
	GroupType category.GroupType //牌型
	Value     uint               //比較值
	RNGs      []uint             //RNG 值
}

func Core2Hand(c *Core) *Hand {
	r := &Hand{
		Group:     c.Group,
		Count:     c.Count,
		GroupType: c.GroupType,
		Value:     c.MaxValue,
		RNGs:      []uint{},
	}

	for _, g := range c.Group {
		r.RNGs = append(r.RNGs, g...)
	}

	return r
}

func (o *Hand) Message() string {
	cat := o.GroupType.ToString()
	rank := cards.Rank(o.Value)
	mc := "["
	kc := ""
	switch o.GroupType {
	case category.Solo, category.Pair, category.Trio, category.Trio1, category.Trio2, category.FourBomb, category.Four1, category.Four2:
		for v, rs := range o.Group {
			if v == o.Value {
				for _, r := range rs {
					mc += cards.Name(r) + ","
				}
			} else {
				for _, r := range rs {
					kc += cards.Name(r) + ","
				}
			}
		}
	case category.Chain, category.PairsChain, category.Airplane:
		min := o.Value - uint(len(o.Group)) + 1
		for i := uint(3); i <= cards.TWO; i++ {
			if i >= min && i <= o.Value {
				for _, r := range o.Group[i] {
					mc += cards.Name(r) + ","
				}
			}
		}
	case category.Airplane1, category.Airplane2:
		d := 4
		if o.GroupType == category.Airplane2 {
			d = 5
		}
		min := o.Value - uint(o.Count/d) + 1
		for i := uint(3); i <= cards.TWO; i++ {
			if i >= min && i <= o.Value {
				for _, r := range o.Group[i] {
					mc += cards.Name(r) + ","
				}
			} else {
				for _, r := range o.Group[i] {
					kc += cards.Name(r) + ","
				}
			}
		}
	default:
		for _, r := range o.RNGs {
			mc += cards.Name(r) + ","
		}
	}
	mc += "]"
	if len(kc) > 0 {
		kc = "+[" + kc + "]"
	}

	return fmt.Sprintf("牌型: %s-%s，總共有 %d 張牌，組成內容：%s", cat, rank, o.Count, mc+kc)
}
