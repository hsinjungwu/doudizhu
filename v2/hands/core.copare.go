package hands

import "ddzSimple/common/category"

func (o *Core) IsBiggerThan(h *Core) bool {
	return o.compare(h) == compBigger
}

func (o *Core) IsSmallerThan(h *Core) bool {
	return o.compare(h) == compSmaller
}

func (o *Core) IsEqual(h *Core) bool {
	return o.compare(h) == compEqual
}

func (o *Core) IsComparable(h *Core) bool {
	return o.compare(h) != compNone
}

func (o *Core) IsNotSmallerThan(h *Core) bool {
	return o.compare(h) != compSmaller
}

type compareType int

const (
	compNone compareType = iota
	compSmaller
	compEqual
	compBigger
)

// 比較大小
func (o *Core) compare(h *Core) compareType {
	if o.GroupType == category.KingBomb {
		return compBigger
	}
	if h.GroupType == category.KingBomb {
		return compSmaller
	}
	if o.GroupType == h.GroupType {
		if o.Count == h.Count {
			if o.MaxValue > h.MaxValue {
				return compBigger
			} else if o.MaxValue < h.MaxValue {
				return compSmaller
			} else {
				return compEqual
			}
		}
	} else {
		if o.GroupType == category.FourBomb {
			return compBigger
		} else if h.GroupType == category.FourBomb {
			return compSmaller
		}
	}
	return compNone
}
