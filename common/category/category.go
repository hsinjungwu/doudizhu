package category

/*
	category 只處理牌型
*/

type GroupType int //牌型

const (
	Unknown    GroupType = iota //未知
	Solo                        //單牌
	Pair                        //對牌
	Trio                        //三張
	Trio1                       //三帶一
	Trio2                       //三帶二
	Chain                       //單順
	PairsChain                  //連對
	Airplane                    //飛機(三順)
	Airplane1                   //飛機帶翅膀一
	Airplane2                   //飛機帶翅膀二
	Four1                       //四帶二(兩張單牌)
	Four2                       //四帶二(兩組一對)
	FourBomb                    //炸彈
	KingBomb                    //王炸
)

func (o GroupType) ToString() string {
	switch o {
	case Solo:
		return "單張"
	case Pair:
		return "對子"
	case Trio:
		return "三條"
	case Trio1:
		return "三帶一"
	case Trio2:
		return "三帶二"
	case Chain:
		return "順子"
	case PairsChain:
		return "連對"
	case Four1:
		return "四帶二(兩張單牌)"
	case Four2:
		return "四帶二(兩組一對)"
	case Airplane:
		return "飛機"
	case Airplane1:
		return "飛機帶1"
	case Airplane2:
		return "飛機帶2"
	case FourBomb:
		return "四炸"
	case KingBomb:
		return "王炸"
	default:
		return "未知"
	}
}
