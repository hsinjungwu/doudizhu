package cards

/*
	cards 只處理單張牌
*/
import (
	"strconv"
)

const (
	A     uint = 14
	TWO   uint = 15
	BJ    uint = 16
	RJ    uint = 17
	rngBJ uint = 52
	rngRJ uint = 53
)

func Rank(v uint) string {
	switch v {
	case A:
		return "A"
	case TWO:
		return "2"
	case BJ:
		return "BJ"
	case RJ:
		return "RJ"
	case 10:
		return "T"
	case 11:
		return "J"
	case 12:
		return "Q"
	case 13:
		return "K"
	default:
		return strconv.Itoa(int(v))
	}
}

func Name(v uint) string {
	switch v {
	case rngBJ:
		return "B🃏"
	case rngRJ:
		return "R🃏"
	}
	suit := "♠"
	switch v % 4 {
	case 1:
		suit = "♥"
	case 2:
		suit = "♣"
	case 3:
		suit = "♦"
	}
	return suit + Rank(getRNGValue(v))

}

// 將 RNG 依照點數分類
func GetValues(rngs []uint) map[uint][]uint {
	m := make(map[uint][]uint)
	for _, rng := range rngs {
		k := getRNGValue(rng)
		m[k] = append(m[k], rng)
	}
	return m
}

func GetKingBombMap() map[uint][]uint {
	m := make(map[uint][]uint)
	m[RJ] = []uint{rngRJ}
	m[BJ] = []uint{rngBJ}
	return m
}

func getRNGValue(rng uint) uint {
	switch rng {
	case rngBJ:
		return BJ
	case rngRJ:
		return RJ
	default:
		v := rng/4 + 1
		switch v {
		case 1:
			return A
		case 2:
			return TWO
		default:
			return v
		}
	}
}

// 根據 RNG 決定牌型分數
func GetBidScore(rngs []uint) int {
	values := GetValues(rngs)
	score := 0
	jk := len(values[RJ])*10 + len(values[BJ])
	switch jk {
	case 11:
		score += 8
	case 10:
		score += 4
	case 1:
		score += 3
	}

	score += 2 * len(values[TWO])
	for i := uint(3); i < TWO; i++ {
		if len(values[i]) == 4 {
			score += 6
		}
	}
	return score
}
