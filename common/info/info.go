package info

import "fmt"

type Info struct {
	MyRNGs      []uint //我的手牌
	PrevRNGs    []uint //上家手牌
	NextRNGs    []uint //下家手牌
	DiscardRNGs []uint //已出過的牌
	CurCardRNGs []uint //這把打的手牌
	FriendOrder int    //同伴順序: 上家=-1, 沒有=0, 下家=+1
	CurPlayer   int    //要跟的牌型是那一家打的。上家=-1, 沒有=0, 下家=+1
}

func NewInfo() *Info {
	result := &Info{
		MyRNGs:      []uint{},
		PrevRNGs:    []uint{},
		NextRNGs:    []uint{},
		DiscardRNGs: []uint{},
		CurCardRNGs: []uint{},
		FriendOrder: 0,
		CurPlayer:   0,
	}
	return result
}

func DebugPrint(format string, a ...interface{}) {
	return
	if len(a) == 0 {
		fmt.Printf("%s\n", format)
	} else {
		fmt.Printf(format+"\n", a...)
	}
}
