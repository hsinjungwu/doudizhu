package main

import (
	"ddzSimple/common/cards"
	"math/rand"
	"os"
	"os/exec"
	"sort"
	"time"
)

func main() {
	loop := 10000000
	simulation(loop)
}

func clear() {
	cmd := exec.Command("clear") //Linux example, its tested
	cmd.Stdout = os.Stdout
	cmd.Run()
}

func shuffle() []uint {
	// return []uint{28, 8, 6, 37, 51, 13, 11, 15, 42, 33, 20, 48, 9, 53, 3, 52, 7, 34, 10, 46, 25, 36, 24, 40, 21, 26, 22, 16, 47, 1, 5, 27, 43, 17, 35, 32, 18, 44, 23, 30, 0, 39, 29, 49, 12, 2, 45, 4, 31, 50, 14, 38, 41, 19}
	rngs := make([]uint, 54)
	for i := 0; i < 54; i++ {
		rngs[i] = uint(i)
	}
	rand.Seed(time.Now().UnixNano())
	rand.Shuffle(len(rngs), func(i, j int) { rngs[i], rngs[j] = rngs[j], rngs[i] })
	return rngs
}

func listCards(rngs []uint) string {
	sort.Slice(rngs, func(i, j int) bool { return rngs[i] < rngs[j] })
	s := "["
	for _, r := range rngs {
		s += cards.Name(r) + ","
	}
	s += "]"
	return s
}
