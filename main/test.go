package main

import (
	"ddzSimple/common/category"
	"ddzSimple/common/info"
	"ddzSimple/dbg"
	"ddzSimple/v2/hands"
	"ddzSimple/v2/strategies"
	"fmt"
	"strings"
	"time"
)

func simulation(loop int) {
	start := time.Now()
	step := loop / 100
	for i := 0; i < loop; i++ {
		rngs := shuffle()
		_, _, err := game(rngs)
		if err != nil {
			fmt.Printf("rngs := %s\n", strings.Join(strings.Fields(fmt.Sprint(rngs)), ","))
			fmt.Printf("發生錯誤%s\n", err.Error())
			// dbgPrint(err.Error())
			fmt.Scanln()
			// } else {
			// clear()
		}
		if i%step == 0 {
			fmt.Printf("Run #%d sim\n", i/step+1)
		}
	}
	elapsed := time.Since(start)

	fmt.Printf("測試耗時 %.6f 分\n", elapsed.Minutes())
}

func game(rngs []uint) (int, bool, error) {
	dbg.Print("rngs := %s", strings.Join(strings.Fields(fmt.Sprint(rngs)), ","))
	players := make([][]uint, 3)

	for i := 0; i < len(rngs)-3; i += 3 {
		for j := 0; j < len(players); j++ {
			players[j] = append(players[j], rngs[i+j])
		}
	}

	desk := rngs[51:]

	landlordId := -1
	landlordStake := -1
	for i := 0; i < len(players); i++ {
		stake := strategies.BidLandlord(players[i], desk, landlordStake)
		if stake > landlordStake {
			landlordId, landlordStake = i, stake
		}
	}
	if landlordStake == 0 {
		dbg.Print("此局流局")
		return -1, false, nil
	} else {
		dbg.Print("莊家為 %d 號玩家", landlordId)
	}

	for _, r := range desk {
		players[landlordId] = append(players[landlordId], r)
	}

	// landlordId = 0
	// players[0] = []uint{45, 46, 48, 49, 0, 3}
	// players[1] = []uint{3, 6, 9, 10, 17, 22, 25, 26, 30, 31, 33, 34, 37, 38, 40, 41, 46}
	// players[2] = []uint{0, 1, 2, 7, 11, 14, 15, 18, 19, 23, 27, 35, 39, 42, 43, 47, 50, 51, 52, 53}

	curID := landlordId
	fo := 1
	passCnt := 0
	curPlayer := 0
	curCore := &hands.Core{
		Group:     map[uint][]uint{},
		Count:     0,
		GroupType: category.Unknown,
		MaxValue:  0,
		MinValue:  0,
	}
	var discardRngs []uint
	for {
		dbg.Print("===================")
		role := "農民"
		if curID == landlordId {
			role = "地主"
		}
		dbg.Print("輪到%d號%s玩家，出牌前手牌剩 %d 張 : %v", curID, role, len(players[curID]), players[curID])
		gi := &info.Info{
			MyRNGs:      players[curID],
			PrevRNGs:    players[(curID+2)%3],
			NextRNGs:    players[(curID+1)%3],
			DiscardRNGs: discardRngs,
			FriendOrder: fo%3 - 1,
			CurCardRNGs: curCore.RNGs(),
			CurPlayer:   curPlayer,
		}
		b, g, e := strategies.Play(gi)
		if e != nil {
			return -1, false, e
		}

		if gi.CurPlayer == 0 {
			dbg.Print("我開新局")
		} else {
			who := "上家"
			if gi.CurPlayer == 1 {
				who = "下家"
			}
			if gi.CurPlayer == gi.FriendOrder {
				who += "友軍"
			} else {
				who += "敵軍"
			}
			dbg.Print("%s打出%s", who, curCore.Message())
		}

		if b {
			dbg.Print("決定出%s", g.Message())
			if gi.CurPlayer != 0 && !g.IsBiggerThan(curCore) {
				return -1, false, fmt.Errorf("牌型有誤！！")
			}
			curCore = g
			var tmp []uint
			for _, r := range players[curID] {
				used := false
				for _, ur := range g.RNGs() {
					if r == ur {
						used = true
						break
					}
				}
				if !used {
					tmp = append(tmp, r)
				}
			}
			players[curID] = tmp
			passCnt = 0
			curPlayer = -1
		} else {
			dbg.Print("選擇 Pass")
			passCnt++
			if passCnt == 2 {
				curCore = &hands.Core{
					Group:     map[uint][]uint{},
					Count:     0,
					GroupType: category.Unknown,
					MaxValue:  0,
					MinValue:  0,
				}
				passCnt = 0
				curPlayer = 0
			} else if passCnt == 1 {
				curPlayer = 1
			}
		}
		dbg.Print("行動結束後手牌剩 %d 張。", len(players[curID]))

		if len(players[curID]) == 0 {
			// fmt.Scanln()
			return curID, curID == landlordId, nil
		}
		curID = (curID + 1) % 3
		fo++
	}
}
