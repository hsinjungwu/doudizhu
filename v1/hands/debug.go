package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"fmt"
	"sort"
)

func (o *Hand) Message() string {
	cat := o.GroupType.ToString()
	rank := cards.Rank(o.Value)
	mc := "["
	kc := ""
	switch o.GroupType {
	case category.Solo, category.Pair, category.Trio, category.Trio1, category.Trio2, category.FourBomb, category.Four1, category.Four2:
		for v, rs := range o.Group {
			if v == o.Value {
				for _, r := range rs {
					mc += cards.Name(r) + ","
				}
			} else {
				for _, r := range rs {
					kc += cards.Name(r) + ","
				}
			}
		}
	case category.Chain, category.PairsChain, category.Airplane:
		min := o.Value - uint(len(o.Group)) + 1
		for i := uint(3); i <= cards.TWO; i++ {
			if i >= min && i <= o.Value {
				for _, r := range o.Group[i] {
					mc += cards.Name(r) + ","
				}
			}
		}
	case category.Airplane1, category.Airplane2:
		d := 4
		if o.GroupType == category.Airplane2 {
			d = 5
		}
		min := o.Value - uint(o.Count/d) + 1
		for i := uint(3); i <= cards.TWO; i++ {
			if i >= min && i <= o.Value {
				for _, r := range o.Group[i] {
					mc += cards.Name(r) + ","
				}
			} else {
				for _, r := range o.Group[i] {
					kc += cards.Name(r) + ","
				}
			}
		}
	default:
		for _, r := range o.RNGs {
			mc += cards.Name(r) + ","
		}
	}
	mc += "]"
	if len(kc) > 0 {
		kc = "+[" + kc + "]"
	}

	return fmt.Sprintf("牌型: %s-%s，總共有 %d 張牌，組成內容：%s", cat, rank, o.Count, mc+kc)
}

func IsValidHandRNGs(rngs []uint) (*Hand, error) {
	if len(rngs) == 0 {
		return &Hand{
			Group:     map[uint][]uint{},
			Count:     0,
			GroupType: 0,
			Value:     0,
			RNGs:      []uint{},
		}, nil
	}

	vs := cards.GetValues(rngs)
	errLog := fmt.Sprintf("錯誤 RNGs = %v, vMap = %v\n", rngs, vs)
	r := &Hand{
		Group: vs,
		RNGs:  rngs,
		Count: len(rngs),
	}

	if len(vs) == 2 && len(vs[cards.BJ])+len(vs[cards.RJ]) == 2 {
		r.Value = cards.RJ
		r.GroupType = category.KingBomb
		return r, nil
	}

	cnts := make([][]uint, 5)
	ls := make([]int, 5)
	for i := 0; i < 5; i++ {
		cnts[i] = []uint{}
	}
	for v, rs := range vs {
		l := len(rs)
		cnts[l] = append(cnts[l], v)
		ls[l]++
	}

	if len(vs) == 1 {
		r.Value = cnts[r.Count][0]
		switch r.Count {
		case 1:
			r.GroupType = category.Solo
		case 2:
			r.GroupType = category.Pair
		case 3:
			r.GroupType = category.Trio
		case 4:
			r.GroupType = category.FourBomb
		default:
			errLog += fmt.Sprintf("相同的 v = %d, 有 %d 個 RNG", r.Value, r.Count)
			return nil, fmt.Errorf(errLog)
		}
		return r, nil
	}

	if ls[1]*ls[2] > 0 || ls[4] > 1 || ls[4]*ls[3] > 0 {
		errLog += fmt.Sprint("發生無效的情況\n")
		return nil, fmt.Errorf(errLog)
	}

	if ls[4] == 1 {
		if len(vs) != 3 || ls[1] == ls[2] {
			errLog += fmt.Sprint("檢查四帶發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}
		r.Value = cnts[4][0]
		r.GroupType = category.Four1
		if ls[2] == 2 {
			r.GroupType = category.Four2
		}
		return r, nil
	}

	fcnt := []int{}
	if ls[3] == 1 {
		if len(vs) > 2 {
			errLog += fmt.Sprint("檢查三帶發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}
		r.Value = cnts[3][0]
		r.GroupType = category.Trio1
		if ls[1] == 0 {
			r.GroupType = category.Trio2
		}
		return r, nil
	}

	if ls[3] >= 2 {
		for _, v := range cnts[3] {
			fcnt = append(fcnt, int(v))
		}
		sort.Ints(fcnt)
		if fcnt[0]+ls[3]-1 != fcnt[ls[3]-1] {
			errLog += fmt.Sprint("檢查飛機發生錯誤\n")
			return nil, fmt.Errorf(errLog)
		}

		r.Value = uint(fcnt[ls[3]-1])
		switch ls[3] {
		case ls[1]:
			r.GroupType = category.Airplane1
		case ls[2]:
			r.GroupType = category.Airplane2
		default:
			r.GroupType = category.Airplane
		}
		return r, nil
	}
	for v := range vs {
		fcnt = append(fcnt, int(v))
	}

	sort.Ints(fcnt)
	l := 0
	if ls[1] >= 5 {
		l = ls[1]
		r.GroupType = category.Chain
	} else if ls[2] >= 3 {
		l = ls[2]
		r.GroupType = category.PairsChain
	}

	if l > 0 && fcnt[0]+l-1 == fcnt[l-1] {
		r.Value = uint(fcnt[l-1])
		return r, nil
	}
	errLog += fmt.Sprint("檢查順發生錯誤\n")
	return nil, fmt.Errorf(errLog)
}

func IsSame(l *Hand, rngs []uint) bool {
	r, e := IsValidHandRNGs(rngs)
	log := fmt.Sprintf("原演算法得到的結果是%s\n", l.Message())
	if e != nil {
		fmt.Printf("%s發生錯誤 : %s\n", log, e.Error())
		return false
	}

	if l.Count != r.Count || l.GroupType != r.GroupType || l.Value != r.Value {
		fmt.Printf("%s新演算法得到的結果是%s\n", log, r.Message())
		return false
	}
	return true
}
