package hands

import (
	"ddzSimple/common/category"
)

// 儲存牌組
type Hand struct {
	Group     map[uint][]uint    //點數對應表, key 點數 / values RNG
	Count     int                //RNG 個數
	GroupType category.GroupType //牌型
	Value     uint               //比較值
	RNGs      []uint             //RNG 值
}

func (o *Hand) IsBiggerThan(h *Hand) bool {
	return o.compare(h) == compBigger
}

func (o *Hand) IsSmallerThan(h *Hand) bool {
	return o.compare(h) == compSmaller
}

func (o *Hand) IsEqual(h *Hand) bool {
	return o.compare(h) == compEqual
}

func (o *Hand) IsNotSmaller(h *Hand) bool {
	return o.compare(h) != compSmaller
}

type compareType int

const (
	compNone compareType = iota
	compSmaller
	compEqual
	compBigger
)

// 比較大小
func (o *Hand) compare(h *Hand) compareType {
	if o.GroupType == category.KingBomb {
		return compBigger
	}
	if h.GroupType == category.KingBomb {
		return compSmaller
	}
	if o.GroupType == h.GroupType {
		if o.Count == h.Count {
			if o.Value > h.Value {
				return compBigger
			} else if o.Value < h.Value {
				return compSmaller
			} else {
				return compEqual
			}
		}
	} else {
		if o.GroupType == category.FourBomb {
			return compBigger
		} else if h.GroupType == category.FourBomb {
			return compSmaller
		}
	}
	return compNone
}
