package hands

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
)

// 回傳王炸牌型
func NewKingBomb() *Hand {
	m := cards.GetKingBombMap()
	var rngs []uint
	for _, rs := range m {
		rngs = append(rngs, rs[0])
	}

	result := &Hand{
		Group:     m,
		GroupType: category.KingBomb,
		Value:     cards.RJ,
		Count:     len(rngs),
		RNGs:      rngs,
	}
	return result
}

// 根據對應值與手牌 RNG 回傳四炸、三條、一對、單張
func NewSameValueCards(v uint, rngs ...uint) *Hand {
	gt := category.Solo
	switch len(rngs) {
	case 4:
		gt = category.FourBomb
	case 3:
		gt = category.Trio
	case 2:
		gt = category.Pair
	}
	result := &Hand{
		Group:     map[uint][]uint{},
		GroupType: gt,
		Value:     v,
		Count:     len(rngs),
		RNGs:      []uint{},
	}
	result.Group[v] = rngs
	result.RNGs = rngs
	return result
}

// 回傳單順、雙順(連對)、飛機
//	cnt: 1 單順, 2 雙順, 3 飛機, vs: 每個 RNG 對應的值, rngs: 手牌 RNG
func NewChains(cnt int, vs []uint, rngs []uint) *Hand {
	gt := category.Chain
	switch cnt {
	case 2:
		gt = category.PairsChain
	case 3:
		gt = category.Airplane
	}
	result := &Hand{
		Group:     map[uint][]uint{},
		GroupType: gt,
		Value:     0,
		Count:     len(rngs),
		RNGs:      []uint{},
	}

	maxV := uint(0)
	for i, v := range vs {
		if v > maxV {
			maxV = v
		}
		result.Group[v] = append(result.Group[v], rngs[i])
		result.RNGs = append(result.RNGs, rngs[i])
	}
	result.Value = maxV
	return result
}
