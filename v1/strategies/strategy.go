package strategies

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/common/info"
	"ddzSimple/v1/core"
	"ddzSimple/v1/hands"
)

// 根據我的手牌 RNG、蓋著 3 張牌的 RNG、目前叫了幾倍，回傳叫牌倍數。
func BidLandlord(myRNGs, deskRNGs []uint, curStake int) int {
	score17 := cards.GetBidScore(myRNGs)
	score20 := cards.GetBidScore(append(myRNGs, deskRNGs...))

	myStake := 0
	if score17 > 7 {
		myStake = 3
	} else if score17 > 5 {
		if score20 > 7 {
			myStake = 3
		}
		myStake = 2
	} else if score17 > 3 {
		if score20 > 5 {
			myStake = 2
		}
		myStake = 1
	}

	if myStake > curStake {
		return myStake
	}
	return 0
}

// 根據遊戲資訊回傳 [是否出牌]、[出什麼牌]、[輸入有無錯誤]
func Play(gi *info.Info) (bool, *hands.Hand, error) {
	err, cg := core.IsValidHandRNGs(gi.CurCardRNGs)
	if err != nil {
		return false, nil, err
	}

	me := core.New(gi.MyRNGs)
	// model.DebugPrint("手牌內容為")
	// for i, mh := range me.Hands {
	// 	model.DebugPrint("第 %d 組為 %s", i, mh.Message())
	// }
	// fmt.Scanln()
	if ok, c := core.IsLastOne(me.Hands); ok && (gi.CurPlayer == 0 || c.IsBiggerThan(cg)) {
		return true, c, nil
	}

	if ok, cs := core.IsBombAndOne(me.Hands); ok {
		if gi.CurPlayer == 0 {
			return true, cs[0], nil
		} else {
			bv := cs[1].Value
			if !core.OtherHasBiggerBomb(bv, gi.NextRNGs) && !core.OtherHasBiggerBomb(bv, gi.PrevRNGs) {
				return true, cs[1], nil
			}
		}
	}

	pMap := cards.GetValues(gi.PrevRNGs)
	pHands := core.SetHands(pMap)
	nMap := cards.GetValues(gi.NextRNGs)
	nHands := core.SetHands(nMap)

	if gi.CurPlayer == 0 {
		if gi.FriendOrder == 0 {
			if ok, c := doLandlordLead(me, pHands, nHands); ok {
				return true, c, nil
			}
		} else {
			if ok, c := doPeasantLead(gi.FriendOrder, cg, me, pHands, nHands); ok {
				return true, c, nil
			}
		}
		return true, me.DoWhenIStart(gi.FriendOrder, pMap, nMap), nil
	} else {
		if gi.FriendOrder == 0 {
			b, g := doLandlordFollow(gi.CurPlayer, cg, me, pHands, nHands)
			return b, g, nil
		} else {
			if gi.FriendOrder == gi.CurPlayer {
				b, g := doPeasantFollowFriend(gi.FriendOrder, cg, me, pHands, nHands)
				return b, g, nil
			} else {
				b, g := doPeasantFollowLandlord(gi.FriendOrder, cg, me, pHands, nHands)
				return b, g, nil
			}
		}
	}
}

func doLandlordLead(m *core.Core, epHands, enHands []*hands.Hand) (bool, *hands.Hand) {
	lastn, nc := core.IsLastOne(enHands)
	lastp, pc := core.IsLastOne(epHands)
	if lastp && lastn { //上下家都各剩一手
		if nc.IsNotSmaller(pc) {
			if ok, c := m.ForceGetNotSmaller(nc); ok {
				return true, c
			}
		} else if nc.IsSmallerThan(pc) {
			if ok, c := m.ForceGetNotSmaller(pc); ok {
				return true, c
			}
		} else {
			tHand := &hands.Hand{}
			tHand.Value = 3
			if nc.Count == 1 || pc.Count == 1 {
				if nc.Count == 2 || pc.Count == 2 {
					tHand.Count = 3
					tHand.GroupType = category.Trio
				} else {
					tHand.Count = 2
					tHand.GroupType = category.Pair
				}
			} else {
				tHand.Count = 1
				tHand.GroupType = category.Solo
			}
			if ok, c := m.ForceGetBiggerOrEqual(tHand); ok {
				return true, c
			}
		}
	} else if lastn {
		if ok, c := m.ForceGetNotSmaller(nc); ok {
			return true, c
		}
	} else if lastp {
		if ok, c := m.ForceGetNotSmaller(pc); ok {
			return true, c
		}
	}
	return false, nil
}

func doLandlordFollow(curPlayer int, cg *hands.Hand, m *core.Core, epHands, enHands []*hands.Hand) (bool, *hands.Hand) {
	lastn, nc := core.IsLastOne(enHands)
	lastp, pc := core.IsLastOne(epHands)

	if lastn && lastp {
		nBig := nc.IsBiggerThan(cg)
		pBig := pc.IsBiggerThan(cg)
		if nBig && pBig {
			if nc.IsBiggerThan(pc) {
				if ok, c := m.ForceGetBiggerOrEqual(nc); ok {
					return true, c
				}
			} else {
				if ok, c := m.ForceGetBiggerOrEqual(pc); ok {
					return true, c
				}
			}
		} else if nBig {
			if ok, c := m.ForceGetBiggerOrEqual(nc); ok {
				return true, c
			}
		} else if pBig {
			if ok, c := m.ForceGetBiggerOrEqual(pc); ok {
				return true, c
			}
		}
	} else if lastn {
		if nc.IsBiggerThan(cg) {
			if ok, c := m.ForceGetBiggerOrEqual(nc); ok {
				return true, c
			}
		} else {
			if curPlayer == 1 {
				if ok, c := m.ForceGetBigger(cg); ok {
					return true, c
				}
			}
		}
	} else if lastp {
		if pc.IsBiggerThan(cg) {
			if ok, c := m.ForceGetBiggerOrEqual(pc); ok {
				return true, c
			}
		} else {
			if curPlayer == -1 {
				if ok, c := m.ForceGetBigger(cg); ok {
					return true, c
				}
			}
		}
	}

	b, g := m.DoWhenEnemyStart(cg)
	return b, g
}

func doPeasantLead(fo int, cg *hands.Hand, m *core.Core, pHands, nHands []*hands.Hand) (bool, *hands.Hand) {
	fHands, eHands := pHands, nHands
	if fo == 1 {
		fHands, eHands = nHands, pHands
	}
	lastF, fc := core.IsLastOne(fHands)
	lastE, ec := core.IsLastOne(eHands)
	if lastE && lastF { //上下家都各剩一手
		if fo == 1 {
			if ok, c := m.ForceGetSmaller(fc); ok {
				return true, c
			}
		} else {
			for _, mh := range m.Hands {
				if mh.IsSmallerThan(fc) && mh.IsNotSmaller(ec) {
					return true, mh
				}
			}
			if ok, c := m.ForceGetNotSmaller(ec); ok {
				return true, c
			}
		}
	} else if lastF {
		if ok, c := m.ForceGetSmaller(fc); ok {
			return true, c
		}
	} else if lastE {
		if ok, c := m.ForceGetNotSmaller(ec); ok {
			return true, c
		}
	}
	return false, nil
}

func doPeasantFollowFriend(fo int, cg *hands.Hand, m *core.Core, pHands, nHands []*hands.Hand) (bool, *hands.Hand) {
	fHands, eHands := pHands, nHands
	if fo == 1 {
		fHands, eHands = nHands, pHands
	}
	lastF, _ := core.IsLastOne(fHands)
	lastE, ec := core.IsLastOne(eHands)
	if lastF && fo == 1 {
		return false, nil
	}
	if lastE && fo == -1 && ec.IsBiggerThan(cg) {
		if ok, c := m.ForceGetBiggerOrEqual(ec); ok {
			return true, c
		}
	}

	return m.DoWhenFriendStart(fo, cg)
}

func doPeasantFollowLandlord(fo int, cg *hands.Hand, m *core.Core, pHands, nHands []*hands.Hand) (bool, *hands.Hand) {
	fHands, eHands := pHands, nHands
	if fo == 1 {
		fHands, eHands = nHands, pHands
	}
	lastF, fc := core.IsLastOne(fHands)
	lastE, ec := core.IsLastOne(eHands)
	if lastE && fo == -1 {
		if ec.IsBiggerThan(cg) {
			if ok, c := m.ForceGetBiggerOrEqual(ec); ok {
				return true, c
			}
		} else {
			if ok, c := m.ForceGetBigger(cg); ok {
				return true, c
			}
		}
	}
	if lastF && fo == 1 && fc.IsBiggerThan(cg) {
		return false, nil
	}

	return m.DoWhenEnemyStart(cg)
}
