package core

/*
	流程邏輯相關
*/

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/v1/hands"
)

type Core struct {
	RNGs     []uint
	Hands    []*hands.Hand
	ValueMap map[uint][]uint
}

func New(iptRNGs []uint) *Core {
	r := &Core{
		RNGs:     iptRNGs,
		ValueMap: map[uint][]uint{},
	}
	vMap := cards.GetValues(iptRNGs)
	for k, v := range vMap {
		r.ValueMap[k] = v
	}
	r.Hands = SetHands(vMap)
	return r
}

func (o *Core) ForceGetBigger(tHand *hands.Hand) (bool, *hands.Hand) {
	if ok, c := o.GetBigger(tHand); ok {
		return true, c
	}

	l := o.Hands[len(o.Hands)-1]
	if tHand.GroupType == category.Solo && tHand.Value == cards.TWO && l.GroupType == category.KingBomb {
		l.GroupType = category.Solo
		l.Count = 1
		l.Value = cards.RJ
		delete(l.Group, cards.BJ)
		l.RNGs = l.Group[cards.RJ]
		return true, l
	}

	fcb := func(i, j uint) bool {
		return i > j
	}
	return forceRankGroup(tHand, o.ValueMap, fcb)
}

func (o *Core) ForceGetSmaller(tHand *hands.Hand) (bool, *hands.Hand) {
	for _, mh := range o.Hands {
		if mh.IsSmallerThan(tHand) {
			return true, mh
		}
	}

	fcs := func(i, j uint) bool {
		return i < j
	}
	return forceRankGroup(tHand, o.ValueMap, fcs)
}

func (o *Core) ForceGetBiggerOrEqual(tHand *hands.Hand) (bool, *hands.Hand) {
	for _, mh := range o.Hands {
		if mh.IsEqual(tHand) {
			return true, mh
		}
	}
	equal := func(i, j uint) bool {
		return i == j
	}

	if ok, c := forceRankGroup(tHand, o.ValueMap, equal); ok {
		return true, c
	}

	return o.ForceGetBigger(tHand)
}

func (o *Core) ForceGetNotSmaller(tHand *hands.Hand) (bool, *hands.Hand) {
	for _, mh := range o.Hands {
		if mh.IsNotSmaller(tHand) {
			return true, mh
		}
	}

	ne := func(i, j uint) bool {
		return i != j
	}
	vMap := cards.GetValues(o.RNGs)
	switch tHand.GroupType {
	case category.Solo, category.Trio1, category.Airplane1, category.Four1, category.Chain: //出一對
		return forceSameKind(2, 0, vMap, ne)
	default:
		return forceSameKind(1, 0, vMap, ne)
	}
}

func (o *Core) GetBigger(curHand *hands.Hand) (bool, *hands.Hand) {
	for _, c := range o.Hands {
		if c.IsBiggerThan(curHand) {
			return true, c
		}
	}
	return false, nil
}

// 玩家自己上手時的策略
func (o *Core) DoWhenIStart(fo int, pMap, nMap map[uint][]uint) *hands.Hand {
	if len(o.Hands) == 2 {
		fcb := func(i, j uint) bool {
			return i > j
		}

		chk := []bool{true, true}
		if fo < 1 {
			for i, mh := range o.Hands {
				has, _ := forceRankGroup(mh, nMap, fcb)
				chk[i] = chk[i] && has
			}
		}
		if fo > -1 {
			for i, mh := range o.Hands {
				has, _ := forceRankGroup(mh, pMap, fcb)
				chk[i] = chk[i] && has
			}
		}

		for i, b := range chk {
			if b {
				return o.Hands[i]
			}
		}
	}

	for _, c := range o.Hands {
		switch c.GroupType {
		case category.FourBomb, category.KingBomb:
			continue
		default:
			if c.Value < cards.A {
				return c
			}
		}
	}

	return o.Hands[0]
}

// 同伴出牌判斷是否要跟
func (o *Core) DoWhenFriendStart(fo int, curHand *hands.Hand) (bool, *hands.Hand) {
	if ok, c := o.GetBigger(curHand); ok {
		switch c.GroupType {
		case category.FourBomb, category.KingBomb:
			return false, nil
		default:
			if len(o.Hands) <= 2 || fo == -1 || c.Value < cards.A {
				return true, c
			}
		}
	}
	return false, nil
}

// 敵人出牌判斷是否要跟
func (o *Core) DoWhenEnemyStart(curHand *hands.Hand) (bool, *hands.Hand) {
	if ok, c := o.GetBigger(curHand); ok {
		if curHand.GroupType == c.GroupType {
			return true, c
		} else if c.GroupType == category.KingBomb {
			if curHand.Value == cards.TWO {
				if curHand.Count > 1 {
					return true, c
				} else {
					c.Count--
					c.GroupType = category.Solo
					delete(c.Group, cards.BJ)
					c.RNGs = c.Group[cards.RJ]
					return true, c
				}
			}
		} else if c.GroupType == category.FourBomb {
			if curHand.Value == cards.A && c.Value == cards.TWO && curHand.Count < 4 {
				c.Count = curHand.Count
				c.GroupType = curHand.GroupType
				c.Group[cards.TWO] = c.Group[cards.TWO][:curHand.Count]
				c.RNGs = c.Group[cards.TWO]
				return true, c
			} else {
				return true, c
			}
		}
	}

	if curHand.Value >= cards.A {
		return false, nil
	}

	switch curHand.GroupType {
	case category.Solo:
		return o.splitSolo(curHand.Value)
	case category.Pair:
		return o.splitPair(curHand.Value)
	case category.Chain, category.PairsChain, category.Airplane:
		return o.splitChain(curHand.GroupType, curHand.Value, len(curHand.Group))
	case category.Trio, category.Trio1, category.Trio2:
		return o.splitTrioOrPlus(curHand.GroupType, curHand.Value)

	default:
		return false, nil
	}
}
