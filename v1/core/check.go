package core

/*
	撰寫所有相關檢查函數
*/

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/v1/hands"
	"fmt"
)

// 手牌是否可組成唯一一組
func IsLastOne(arrHands []*hands.Hand) (bool, *hands.Hand) {
	if len(arrHands) == 1 {
		return true, arrHands[0]
	}

	if len(arrHands) != 3 { //無法組成四帶一、四帶二
		return false, nil
	}

	a0 := arrHands[0]
	a1 := arrHands[1]
	a2 := arrHands[2]

	if a2.GroupType != category.FourBomb || a0.GroupType != a1.GroupType {
		return false, nil
	}

	kMap := make(map[uint][]uint)
	kMap[a0.Value] = arrHands[0].RNGs
	kMap[a1.Value] = arrHands[1].RNGs
	switch a0.GroupType {
	case category.Solo:
		return true, mergeGroupWithKicker(category.Four1, a2, kMap)
	case category.Pair:
		return true, mergeGroupWithKicker(category.Four2, a2, kMap)
	default:
		return false, nil
	}
}

// 剩下的牌型是否為王炸/炸彈 + 任意一組
func IsBombAndOne(arrHands []*hands.Hand) (bool, []*hands.Hand) {
	l := len(arrHands)
	b := arrHands[l-1]
	switch b.GroupType {
	case category.KingBomb, category.FourBomb:
		if ok, c := IsLastOne(arrHands[:l-1]); ok {
			return true, []*hands.Hand{c, b}
		}
	}
	return false, nil
}

// 其他人是否有比我大的炸彈
func OtherHasBiggerBomb(myBombV uint, orngs []uint) bool {
	ov := cards.GetValues(orngs)
	if len(ov[cards.RJ])+len(ov[cards.BJ]) == 2 {
		return true
	}

	for v := myBombV + 1; v <= cards.TWO; v++ {
		if len(ov[v]) == 4 {
			return true
		}
	}
	return false
}

// RNG 是否能有效組成一個牌組
func IsValidHandRNGs(rngs []uint) (error, *hands.Hand) {
	if len(rngs) == 0 {
		return nil, &hands.Hand{
			Group:     map[uint][]uint{},
			Count:     0,
			GroupType: category.Unknown,
			Value:     0,
			RNGs:      rngs,
		}
	}
	vm := cards.GetValues(rngs)
	log := fmt.Sprintf("分類表為 %v", vm)
	rh := SetHands(vm)
	if ok, c := IsLastOne(rh); ok {
		return nil, c
	}

	return fmt.Errorf("rngs 為 %v, %s\n", rngs, log), nil
}
