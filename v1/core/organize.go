package core

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/v1/hands"
	"sort"
)

func SetHands(valueMap map[uint][]uint) []*hands.Hand {
	r := arrangeGroups(valueMap)
	r = mergeGroups(r)
	sortGroups(r)
	return r
}

// 基本理牌，順序為 王炸 => 炸彈 → 單順 → 雙順 → 三順、三條 → 一對 → 單張
func arrangeGroups(valueMap map[uint][]uint) []*hands.Hand {
	var result []*hands.Hand

	if len(valueMap[cards.BJ])+len(valueMap[cards.RJ]) == 2 {
		result = append(result, hands.NewKingBomb())
		delete(valueMap, cards.BJ)
		delete(valueMap, cards.RJ)
	}

	for v, r := range valueMap {
		if len(r) == 4 {
			result = append(result, hands.NewSameValueCards(v, r...))
			delete(valueMap, v)
		}
	}

	if r, has := valueMap[cards.TWO]; has {
		result = append(result, hands.NewSameValueCards(cards.TWO, r...))
		delete(valueMap, cards.TWO)
	}

	minLenChain := uint(5)
	var valueSoloChains [][]uint
	var rngSoloChains [][]uint
	for v := uint(3); v <= 10; v++ {
		lr := len(valueMap[v])
		for l := 0; l < lr; l++ {
			t := 1
			for i := uint(0); i < minLenChain; i++ {
				t *= len(valueMap[v+i])
			}
			if t > 0 {
				tmpV := make([]uint, minLenChain)
				tmpR := make([]uint, minLenChain)
				for i := uint(0); i < minLenChain; i++ {
					tmpV[i] = v + i
					tmpR[i] = valueMap[v+i][0]
					valueMap[v+i] = valueMap[v+i][1:]
				}
				valueSoloChains = append(valueSoloChains, tmpV)
				rngSoloChains = append(rngSoloChains, tmpR)
			}
		}
	}

	for i := 0; i < len(valueSoloChains); i++ { //將剩餘牌盡量延長
		sc := valueSoloChains[i]
		nv := sc[len(sc)-1] + 1
		for nv <= cards.A && len(valueMap[nv]) > 0 {
			valueSoloChains[i] = append(valueSoloChains[i], nv)
			rngSoloChains[i] = append(rngSoloChains[i], valueMap[nv][0])
			valueMap[nv] = valueMap[nv][1:]
			nv++
		}
	}
	// fmt.Printf("對接前\n")
	// for i, s := range valueSoloChains {
	// 	fmt.Printf("#%d = %v\n", i, s)
	// }
	//單順對接
	for i := 0; i < len(valueSoloChains); i++ {
		si := valueSoloChains[i]
		fi := si[0]
		if fi == 0 {
			continue
		}
		li := si[len(si)-1]
		for j := 0; j < len(valueSoloChains); j++ {
			if i == j {
				continue
			}
			sj := valueSoloChains[j]
			fj := sj[0]
			if fj == 0 {
				continue
			}
			lj := sj[len(sj)-1]
			if lj+1 == fi {
				valueSoloChains[j] = append(valueSoloChains[j], valueSoloChains[i]...)
				rngSoloChains[j] = append(rngSoloChains[j], rngSoloChains[i]...)
				valueSoloChains[i][0] = 0
				break
			} else if li+1 == fj {
				valueSoloChains[i] = append(valueSoloChains[i], valueSoloChains[j]...)
				rngSoloChains[i] = append(rngSoloChains[i], rngSoloChains[j]...)
				valueSoloChains[j][0] = 0
				break
			}
		}
	}

	valueSoloChains, rngSoloChains = removeZeroChains(valueSoloChains, rngSoloChains)
	// fmt.Printf("對接後\n")
	// for i, s := range valueSoloChains {
	// 	fmt.Printf("#%d = %v\n", i, s)
	// }
	var pairChains [][]uint
	var rngPairChains [][]uint
	for i := 0; i < len(valueSoloChains); i++ {
		si := valueSoloChains[i]
		if si[0] == 0 {
			continue
		}
		for j := i + 1; j < len(valueSoloChains); j++ {
			sj := valueSoloChains[j]
			if si[0] == sj[0] && len(si) == len(sj) {
				tC := append(si, sj...)
				pairChains = append(pairChains, tC)
				tR := append(rngSoloChains[i], rngSoloChains[j]...)
				rngPairChains = append(rngPairChains, tR)
				valueSoloChains[i][0] = 0
				valueSoloChains[j][0] = 0
			}
		}
	}
	valueSoloChains, rngSoloChains = removeZeroChains(valueSoloChains, rngSoloChains)

	var airplanes [][]uint
	var rngAirplanes [][]uint
	for i := 0; i < len(valueSoloChains); i++ {
		si := valueSoloChains[i]
		if si[0] == 0 {
			continue
		}
		for j := 0; j < len(pairChains); j++ {
			pj := pairChains[j]
			if si[0] == pj[0] && 2*len(si) == len(pj) {
				tC := append(si, pj...)
				airplanes = append(airplanes, tC)
				tR := append(rngSoloChains[i], rngPairChains[j]...)
				rngAirplanes = append(rngAirplanes, tR)
				valueSoloChains[i][0] = 0
				pairChains[j][0] = 0
			}
		}
	}
	valueSoloChains, rngSoloChains = removeZeroChains(valueSoloChains, rngSoloChains)
	pairChains, rngPairChains = removeZeroChains(pairChains, rngPairChains)

	for i, c := range valueSoloChains {
		result = append(result, hands.NewChains(1, c, rngSoloChains[i]))
	}

	for i, c := range pairChains {
		result = append(result, hands.NewChains(2, c, rngPairChains[i]))
	}

	for i, c := range airplanes {
		result = append(result, hands.NewChains(3, c, rngAirplanes[i]))
	}

	//搜尋剩下的飛機、雙順
	for cnt := 3; cnt > 1; cnt-- { //該牌的張數
		for ln := 4; ln > 4-cnt; ln-- { //牌組長度
			for v := 3; v <= 15-ln; v++ {
				has := true
				for j := 0; j < ln; j++ {
					has = has && len(valueMap[uint(v+j)]) >= cnt
				}
				if has {
					var vs []uint
					var rngs []uint
					for j := 0; j < ln; j++ {
						for l := 0; l < cnt; l++ {
							vs = append(vs, uint(v+j))
						}
						rngs = append(rngs, valueMap[uint(v+j)][:cnt]...)
						valueMap[uint(v+j)] = valueMap[uint(v+j)][cnt:]
					}
					result = append(result, hands.NewChains(cnt, vs, rngs))
				}
			}
		}
	}
	//搜尋剩下的三條、一對、單牌
	for cnt := 3; cnt > 0; cnt-- { //該牌的張數
		for v := uint(3); v <= cards.RJ; v++ {
			if len(valueMap[v]) == cnt {
				result = append(result, hands.NewSameValueCards(v, valueMap[v]...))
				valueMap[v] = valueMap[v][cnt:]
			}
		}
	}
	return result
}

// 移除無效組合
func removeZeroChains(tC, tR [][]uint) ([][]uint, [][]uint) {
	var rC [][]uint
	var rR [][]uint
	for i, c := range tC {
		if c[0] > 0 {
			rC = append(rC, c)
			rR = append(rR, tR[i])
		}
	}
	return rC, rR
}

// 將基本牌型合成三帶、飛機帶，給出最終牌組
func mergeGroups(arrHand []*hands.Hand) []*hands.Hand {
	var result []*hands.Hand
	trios := make(map[uint]*hands.Hand)
	airplains := make(map[uint]*hands.Hand)
	pairs := make(map[uint]*hands.Hand)
	solos := make(map[uint]*hands.Hand)
	for _, h := range arrHand {
		switch h.GroupType {
		case category.Solo:
			if h.Value <= cards.TWO {
				solos[h.Value] = h
			} else {
				result = append(result, h)
			}
		case category.Pair:
			pairs[h.Value] = h
		case category.Trio:
			trios[h.Value] = h
		case category.Airplane:
			airplains[h.Value] = h
		default:
			result = append(result, h)
		}
	}

	result = append(result, mergeTrioAirplanePlus(category.Airplane1, solos, airplains)...)
	result = append(result, mergeTrioAirplanePlus(category.Airplane2, pairs, airplains)...)
	result = append(result, mergeTrioAirplanePlus(category.Trio1, solos, trios)...)
	result = append(result, mergeTrioAirplanePlus(category.Trio2, pairs, trios)...)

	for _, h := range solos {
		result = append(result, h)
	}

	for _, h := range pairs {
		result = append(result, h)
	}

	for _, h := range trios {
		result = append(result, h)
	}

	for _, h := range airplains {
		result = append(result, h)
	}
	return result
}

// 由三條、三順組合三帶一、二跟飛機一、二。
func mergeTrioAirplanePlus(gt category.GroupType, kickers, masters map[uint]*hands.Hand) []*hands.Hand {
	var result []*hands.Hand
	for mv := uint(3); mv <= cards.TWO; mv++ {
		if mh, mok := masters[mv]; mok {
			ml := mh.Count / 3
			if len(kickers) >= ml {
				r := &hands.Hand{
					Count:     mh.Count,
					RNGs:      mh.RNGs,
					Value:     mh.Value,
					Group:     mh.Group,
					GroupType: gt,
				}
				for sv := uint(3); sv <= cards.TWO; sv++ {
					if sh, sok := kickers[sv]; sok {
						r.Count += sh.Count
						r.Group[sv] = sh.RNGs
						r.RNGs = append(r.RNGs, sh.RNGs...)
						delete(kickers, sv)
						ml--
						if ml == 0 {
							break
						}
					}
				}
				delete(masters, mv)
				result = append(result, r)
			}
		}
	}
	return result
}

// 合成三帶一二、飛機一二跟四帶一二
//  gt: 指定牌型, mHand: 主體牌型, kMap: kicker 點數對照表
func mergeGroupWithKicker(gt category.GroupType, mHand *hands.Hand, kMap map[uint][]uint) *hands.Hand {
	mHand.GroupType = gt
	for v, rs := range kMap {
		mHand.Count += len(rs)
		mHand.Group[v] = rs
		mHand.RNGs = append(mHand.RNGs, rs...)
	}
	return mHand
}

// 排序
func sortGroups(arr []*hands.Hand) {
	sort.Slice(arr, func(i1, i2 int) bool {
		c1 := arr[i1]
		c2 := arr[i2]
		if c1.GroupType == c2.GroupType {
			if c1.Value == c2.Value {
				return c1.Count < c2.Count
			}
			return c1.Value < c2.Value
		}
		return c1.GroupType < c2.GroupType
	})
}
