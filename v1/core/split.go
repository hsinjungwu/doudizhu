package core

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/v1/hands"
)

func (o *Core) splitSolo(rank uint) (bool, *hands.Hand) {
	vMap := make(map[uint][]uint)
	for _, mh := range o.Hands {
		switch mh.GroupType {
		case category.Chain:
			if mh.Count > 5 {
				v := mh.Value - uint(mh.Count) + 1
				vMap[v] = mh.Group[v]
				vMap[mh.Value] = mh.Group[mh.Value]
			}
		case category.Pair, category.Trio:
			vMap[mh.Value] = mh.Group[mh.Value][:1]
		}
	}
	for v := rank + 1; v < cards.BJ; v++ {
		if len(vMap[v]) > 0 {
			return true, hands.NewSameValueCards(v, vMap[v]...)
		}
	}
	return false, nil
}

func (o *Core) splitPair(rank uint) (bool, *hands.Hand) {
	vMap := make(map[uint][]uint)
	for _, mh := range o.Hands {
		if mh.GroupType == category.PairsChain && mh.Count > 2*3 {
			v := mh.Value - uint(mh.Count/2) + 1
			vMap[v] = mh.Group[v]
			vMap[mh.Value] = mh.Group[mh.Value]
		} else if mh.GroupType == category.Airplane && mh.Count > 3*2 {
			v := mh.Value - uint(mh.Count/3) + 1
			vMap[v] = mh.Group[v]
			vMap[mh.Value] = mh.Group[mh.Value]
		} else if mh.GroupType == category.Trio {
			vMap[mh.Value] = mh.Group[mh.Value][:2]
		}
	}
	for v := rank + 1; v < cards.BJ; v++ {
		if len(vMap[v]) > 0 {
			return true, hands.NewSameValueCards(v, vMap[v]...)
		}
	}
	return false, nil
}

func (o *Core) splitChain(cgt category.GroupType, rank uint, cnt int) (bool, *hands.Hand) {
	minLengh := 13
	minValue := cards.TWO
	result := &hands.Hand{}
	for _, mh := range o.Hands {
		if mh.GroupType == cgt && mh.Count > cnt && mh.Value > rank {
			if mh.Count < minLengh || (mh.Count == minLengh && mh.Value < minValue) {
				result = mh
				minLengh = mh.Count
				minValue = mh.Value
			}
		}
	}

	if result.GroupType == category.Unknown {
		return false, nil
	}

	for v := range result.Group {
		if v <= rank {
			delete(result.Group, v)
		}
	}

	if len(result.Group) < cnt {
		return false, nil
	}

	for len(result.Group) > cnt {
		delete(result.Group, result.Value)
		result.Value--
	}

	var rngs []uint
	for _, rs := range result.Group {
		rngs = append(rngs, rs...)
	}
	result.RNGs = rngs
	result.Count = len(rngs)
	return true, result
}

func (o *Core) splitTrioOrPlus(tgt category.GroupType, rank uint) (bool, *hands.Hand) {
	vMap := make(map[uint][]uint)
	kMap := make(map[uint][]uint)
	d := 3
	fgt := category.Airplane
	switch tgt {
	case category.Trio1:
		fgt = category.Airplane1
		d = 4
	case category.Trio2:
		fgt = category.Airplane2
		d = 5
	}

	for _, mh := range o.Hands {
		if mh.GroupType == fgt && mh.Count > 3*2 {
			v := mh.Value - uint(mh.Count/d) + 1
			vMap[v] = mh.Group[v]
			vMap[mh.Value] = mh.Group[mh.Value]
			for kv, krs := range mh.Group {
				if len(krs) < 3 {
					kMap[kv] = krs
				}
			}
		}
	}

	result := &hands.Hand{
		Group:     map[uint][]uint{},
		Count:     0,
		GroupType: tgt,
		Value:     0,
		RNGs:      []uint{},
	}

	for v := rank + 1; v <= cards.TWO; v++ {
		if len(vMap[v]) > 0 {
			result.Group[v] = vMap[v]
			result.RNGs = append(result.RNGs, vMap[v]...)
			result.Value = v
			break
		}
	}

	for v := uint(3); v <= cards.TWO; v++ {
		if len(kMap[v]) > 0 {
			result.Group[v] = kMap[v]
			result.RNGs = append(result.RNGs, kMap[v]...)
			break
		}
	}

	if result.Value == 0 {
		return false, nil
	}
	result.Count = len(result.RNGs)
	return true, result
}
