package core

import (
	"ddzSimple/common/cards"
	"ddzSimple/common/category"
	"ddzSimple/v1/hands"
	"fmt"
)

/*
	撰寫所有強拆原始手牌組合的相關函數
*/

// 強拆 RNG 建立指定牌型的牌
func forceRankGroup(eHand *hands.Hand, vMap map[uint][]uint, cp func(uint, uint) bool) (bool, *hands.Hand) {
	switch eHand.GroupType {
	case category.Solo, category.Pair, category.Trio:
		return forceSameKind(eHand.Count, eHand.Value, vMap, cp)
	case category.Trio1, category.Trio2:
		return forceTrioPlus(eHand, vMap, cp)
	case category.Chain, category.PairsChain, category.Airplane:
		return forceStraight(eHand.Count/len(eHand.Group), len(eHand.Group), eHand.Value, vMap, cp)
	case category.Airplane1, category.Airplane2:
		return forceAirplanPlus(eHand, vMap, cp)
	case category.Four1, category.Four2:
		return forceFourPlus(eHand, vMap, cp)
	}

	return false, nil
}

// 根據手牌 rng 強制拆出單張、對子、三條或四炸
//	size: 1 ~ 4, rank: 要比較的 rank, vMap: rng 點數對照表, fc: 強拆比較函數
func forceSameKind(size int, rank uint, vMap map[uint][]uint, fc func(uint, uint) bool) (bool, *hands.Hand) {
	for v, rs := range vMap {
		if fc(v, rank) && len(rs) >= size {
			return true, hands.NewSameValueCards(v, rs[:size]...)
		}
	}
	return false, nil
}

// 根據手牌 rng 強制拆出單順、連對或飛機
//	size: 1 ~ 3, count: 順子長度, rank: 要比較的 rank, vMap: rng 點數對照表, fc: 強拆比較函數
func forceStraight(size, count int, rank uint, vMap map[uint][]uint, fc func(uint, uint) bool) (bool, *hands.Hand) {
	uc := uint(count)
	gt := category.Chain
	switch size {
	case 2:
		gt = category.PairsChain
	case 3:
		gt = category.Airplane
	}
	for v := uint(3); v < cards.TWO; v++ {
		g := &hands.Hand{}
		g.Group = map[uint][]uint{}
		g.Value = v
		g.GroupType = gt
		for i := uint(0); i < uc; i++ {
			if v+i < cards.TWO && len(vMap[v+i]) >= size {
				r := vMap[v+i][:size]
				g.Group[v+i] = r
				g.RNGs = append(g.RNGs, r...)
				g.Count += len(r)
				g.Value = v + i
			}
		}

		if g.Count == size*count && fc(g.Value, rank) {
			return true, g
		}
	}
	return false, nil
}

// 根據手牌 rng 強制拆出三帶一或三帶二
//  curHand: 要比較的牌型, vMap: rng 點數對照表, fc: 強拆比較函數
func forceTrioPlus(curHand *hands.Hand, vMap map[uint][]uint, fc func(uint, uint) bool) (bool, *hands.Hand) {
	ok, m := forceSameKind(3, curHand.Value, vMap, fc)
	if !ok {
		return false, nil
	}
	ok, k := getKickers(curHand.GroupType, 1, m.Value, vMap)
	if !ok {
		return false, nil
	}
	return true, mergeGroupWithKicker(curHand.GroupType, m, k)
}

// 根據手牌 rng 強制拆出飛機一或飛機二
//  curHand: 要比較的牌型, vMap: rng 點數對照表, fc: 強拆比較函數
func forceAirplanPlus(curHand *hands.Hand, vMap map[uint][]uint, fc func(uint, uint) bool) (bool, *hands.Hand) {
	fmt.Print("強拆飛機\n")
	cnt := 0
	if curHand.GroupType == category.Airplane1 {
		cnt = curHand.Count / 4
	} else {
		cnt = curHand.Count / 5
	}

	ok, m := forceStraight(3, cnt, curHand.Value, vMap, fc)
	if !ok {
		return false, nil
	}
	ok, k := getKickers(curHand.GroupType, cnt, m.Value, vMap)
	if !ok {
		return false, nil
	}
	return true, mergeGroupWithKicker(curHand.GroupType, m, k)
}

// 根據手牌 rng 強制拆出四帶一或四帶二
//  curHand: 要比較的牌型, vMap: rng 點數對照表, fc: 強拆比較函數
func forceFourPlus(curHand *hands.Hand, vMap map[uint][]uint, fc func(uint, uint) bool) (bool, *hands.Hand) {
	ok, m := forceSameKind(4, curHand.Value, vMap, fc)
	if !ok {
		return false, nil
	}

	ok, k := getKickers(curHand.GroupType, 2, m.Value, vMap)
	if !ok {
		return false, nil
	}

	return true, mergeGroupWithKicker(curHand.GroupType, m, k)
}

// 取得指定牌型帶的牌
//  gt: 指定牌型, count: 長度。三帶為 1、飛機長度、四帶為 2, rank: 主體 rank, vMap: rng 點數對照表
func getKickers(gt category.GroupType, count int, rank uint, vMap map[uint][]uint) (bool, map[uint][]uint) {
	size := 1
	switch gt {
	case category.Trio2, category.Airplane2, category.Four2:
		size = 2
	}
	kMap := make(map[uint][]uint)
	for i := uint(3); i <= cards.TWO; i++ {
		if rank != i && len(vMap[i]) >= size {
			kMap[i] = vMap[i][:size]
			if len(kMap) == count {
				return true, kMap
			}
		}
	}
	return false, nil
}
