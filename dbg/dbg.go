package dbg

import (
	"ddzSimple/common/category"
	"ddzSimple/v2/hands"
	"fmt"
)

const (
	show bool = false
)

func Print(format string, a ...interface{}) {
	if !show {
		return
	}
	if len(a) == 0 {
		fmt.Printf("%s", format)
	} else {
		fmt.Printf(format, a...)
	}
	fmt.Print("\n")
}

func ShowHands(hands map[category.GroupType][]*hands.Core) {
	if !show {
		return
	}
	fmt.Print("我的手牌有：\n")
	for _, cs := range hands {
		for _, c := range cs {
			fmt.Printf("%s\n", c.Message())
		}
	}
}
